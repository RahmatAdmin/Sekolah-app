<?php
namespace App\Services;

use App\Models\LogNotifikasi;
use App\Models\Orangtua;
use App\Models\Siswa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

use function PHPUnit\Framework\isNull;

class WhatsAppService
{
    private $url;
    private $api_key;
    private $api_secret;

    public function __construct()
    {
        $this->url = env('WHATSAPP_URL');
        $this->api_key = env('WHATSAPP_API_KEY');
        $this->api_secret = env('WHATSAPP_API_SECRET');
    }

    private function send_message($message, $to, $log)
    {
        try {
            $response = Http::withHeaders([
                'x-api-key' => $this->api_key,
                'x-api-secret' => $this->api_secret,
                'Application' => 'application/json',
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ])->post($this->url, [
                'text' => $message,
                'number' => $to . '@s.whatsapp.net',
            ]);

            $data = $response->json();
            $data_log = json_decode($log->log, true) ?? []; // Pastikan selalu array

            if ($data['status'] == 'success') {
                $log->update([
                    'is_success' => 1,
                    'log' => json_encode(array_merge($data_log, [time() => $data])) // Gabungkan array
                ]);
                return;
            } else {
                $log->update([
                    'count' => $log->count + 1,
                    'log' => json_encode(array_merge($data_log, [time() => $data]))
                ]);
                return;
            }
        } catch (\Exception $e) {
            $data_log = json_decode($log->log, true) ?? []; // Ulangi validasi
            $log->update([
                'count' => $log->count + 1,
                'log' => json_encode(array_merge($data_log, [time() => $e->getMessage()]))
            ]);
            return;
        }
    }


    private function get_message($name, $gender, $is_siswa=false, $mode=0)
    {
        // jika ini adalah orang tua
        if ($gender == 'Laki-laki' && !$is_siswa) {
            $name = 'Bapak ' . $name;
        } elseif ($gender == 'Perempuan' && !$is_siswa) {
            $name = 'Ibu ' . $name;
        } elseif ($is_siswa && $gender == 'Laki-laki') {
            $name = 'Saudara ' . $name;
        } elseif ($is_siswa && $gender == 'Perempuan') {
            $name = 'Saudari ' . $name;
        }

        if ($mode == 0) {
            return <<<EOT
            Assalamu'alaikum Warahmatullahi Wabarakatuh.. Dear {$name},
            👳‍♀️ : Kami ucapkan terima kasih atas kepercayaan Anda dalam memilih Pondok Pesantren Baitul Qur'an Bina Ukhuwah sebagai tempat menimba ilmu. Proses pendaftaran Anda telah berhasil disimpan dan saat ini sedang dalam tahap verifikasi oleh tim kami.
            👳‍♀️ : Kami akan segera memeriksa dan memproses informasi pendaftaran yang telah Anda kirimkan. Proses verifikasi ini mungkin memerlukan waktu beberapa hari. untuk pemberitahuan lebih lanjut mengenai status pendaftaran akan di konfirmasi melalui WhatsApp yang digunakan dalam proses pendaftaran.
            👳‍♀️ : Jika Anda memiliki pertanyaan atau membutuhkan bantuan lebih lanjut, jangan ragu untuk menghubungi kami melalui email baitulquranbinaukhuwah2024@gmail.com .Sekali lagi, terima kasih telah mendaftar dan mempercayai Pondok Pesantren Pondok Pesantren Baitul Qur'an Bina Ukhuwah . Semoga Allah SWT memudahkan langkah kita dalam mencari ilmu dan kebaikan.
            Wassalamu'alaikum Warahmatullahi Wabarakatuh,,, 🙏
            Pondok Pesantren Baitul Qur'an Bina Ukhuwah
            EOT;
        } else {
            return <<<EOT
            Assalamu'alaikum Warahmatullahi Wabarakatuh.. Dear {$name},
            👳‍♀️ : Kami dari Pondok Pesantren Baitul Qur'an Bina Ukhuwah dengan senang hati menginformasikan bahwa Anda telah dinyatakan LULUS seleksi pendaftaran.
            👳‍♀️ : Selamat atas kelulusan Anda! Untuk langkah selanjutnya, mohon untuk segera mengunjungi pondok pesantren kami untuk melakukan tes uji Kompetensi Baca Al Qur'an sebagai tahap akhir menyelesaikan proses administrasi
            Sebagai Arsip Fisik Santri/i
            Mohon membawa berkas fisik :

            1. Fotocopy Akte kelahiran 1 lmbr
            2. Fotocopy KK 1 lmbr
            3. SKL ( bisa menyusul jika belum ada )
            4. Pas Foto 3x4 2 lmbr

            👳‍♀️ : Untuk informasi lebih lanjut. Jika ada pertanyaan, Anda dapat menghubungi kami di
            📱: 0823 6539 9161 ( Ustd. Fahrizal )
            ✉️ : baitulquranbinaukhuwah2024@gmail.com
            👳‍♀️ : sekali lagi selamat atas kelulusan Anda dan kami tunggu kedatangan Anda di Pondok Pesantren Pondok Pesantren Baitul Qur'an Bina Ukhuwah.
            Wassalamu'alaikum Warahmatullahi Wabarakatuh..
            Pondok Pesantren Baitul Qur'an Bina Ukhuwah
            EOT;
        }
    }

    public function send_verif($siswa_id, $mode=0)
    {
        $siswa = Siswa::find($siswa_id);
        if (!$siswa) {
            Log::error('siswa not found with request ' . $siswa_id);
            return;
        }
        $ibu = Orangtua::where('siswa_id', $siswa->id)->where('gender', 'Perempuan')->first();
        $ayah = Orangtua::where('siswa_id', $siswa->id)->where('gender', 'Laki-laki')->first();

        $rt = [];
        $up = 0;

        DB::beginTransaction();
        try {
            $now_time = time();

            if (!is_null($siswa->no_telp)) {
                $siswa_log = LogNotifikasi::create([
                    'orangtua_id' => null,
                    'siswa_id' => $siswa->id,
                    'message' => $this->get_message($siswa->nama_lengkap, $siswa->gender, $mode),
                    'number' => $siswa->no_telp,
                ]);
            }
            $up = 1;

            $ayah_log = LogNotifikasi::create([
                'orangtua_id' => $ayah->id,
                'siswa_id' => $siswa->id,
                'message' => $this->get_message($ayah->full_name, $ayah->gender, $mode),
                'number' => $ayah->phone,
            ]);

            $up = 2;

            $ibu_log = LogNotifikasi::create([
                'orangtua_id' => $ibu->id,
                'siswa_id' => $siswa->id,
                'message' => $this->get_message($ibu->full_name, $ibu->gender, $mode),
                'number' => $ibu->phone,
            ]);

            $up = 3;

            $rt[] = $siswa_log ?? null;
            $rt[] = $ayah_log;
            $rt[] = $ibu_log;
            // Log::info("send_verif " . $up . " : " . json_encode($rt));
            DB::commit();
            return $rt;
        } catch (\Exception $e) {
            Log::error("send_verif " . $up . " : " . $e->getMessage());
            Log::info("data : " . json_encode([$siswa, $ayah, $ibu]));
            DB::rollBack();
            // hany ada kemungkinan error ini, yaitu siswa id dan kedua orang tua datanya tidak ada
            throw new \Exception($e->getMessage());
        }
    }

    public function cron_start()
    {
        $all_log = LogNotifikasi::where('is_success', 0)->where('count', '<', 5)->get();
        foreach ($all_log as $log) {
            $this->send_message($log->message, $log->number, $log);
        }
    }
}
