<?php

namespace App\Console\Commands;

use App\Services\WhatsAppService;
use Illuminate\Console\Command;

class SendWhatsAppNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whatsapp:send-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send WhatsApp notifications every minute';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
        $whatsappService = new WhatsAppService();
        $whatsappService->cron_start();
    }
}
