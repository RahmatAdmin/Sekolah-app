<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Siswa extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'siswa';

    // protected $fillable = [
    //     'nama_lengkap',
    //     'jenis_kelamin',
    //     'tanggal_lahir',
    //     'alamat',
    //     'nisn',
    //     'akte',
    //     'kk',
    //     'skl',
    //     'pas_foto',
    //     'nama_ayah',
    //     'nama_ibu',
    //     'is_accepted_phase_1'
    // ];

    protected $guarded = ['id'];

    public function orangtuas()
    {
        return $this -> hasMany(Orangtua::class, 'siswa_id', 'id');
    }
}
