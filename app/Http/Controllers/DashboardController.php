<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Guru;
use App\Models\Siswa;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  public function index()
    {
        $totalGuru = Guru::count();
        // $totalSiswa = Siswa::where('is_accepted_phase_1', '1')->count();
        $totalSiswa = Siswa::count();
        $totalArtikel = Artikel::count();



        return view('admin.dashboard', compact('totalGuru', 'totalSiswa', 'totalArtikel'));
    }



}
