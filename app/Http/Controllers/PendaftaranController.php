<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PendaftaranController extends Controller
{
    public function index() {
        $casis = Siswa::where('is_accepted_phase_1', 0)->get();

         return view('admin.daftar', compact('casis'));
    }

    
    public function update ($id)
    {
        DB::transaction(function () use ($id) {
            $siswa = Siswa::find($id);

            if ($siswa) {
                $siswa->is_accepted_phase_1 = 1;
                $siswa->save();
            }
        });

        return redirect()->back()->with('success', 'Siswa berhasil diterima.');
    }

}

