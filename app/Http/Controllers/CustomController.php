<?php

namespace App\Http\Controllers;

use App\Models\Custom;
use Illuminate\Http\Request;

class CustomController extends Controller
{
    public function index() {
        $dataset = Custom::all();
        return view('admin.custom', compact('dataset'));
    }

    public function upload(Request $request)
    {
        $request->validate([
            'dropzone-file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'quantity-input' => 'required|string|max:255',
        ]);

        if ($request->hasFile('dropzone-file')) {
            $imageFile = $request->file('dropzone-file');
            $imageName = time() . '1.' . $imageFile->getClientOriginalExtension();
            $imageFile->storeAs('public/', $imageName);

            Custom::create([
                'dropzone-file' => $imageName,
                'quantity-input' => $request->input('quantity-input'),
            ]);

            return redirect('custom-layout')->with('success', 'Data siswa berhasil disimpan.');
        }

        return redirect('siswa')->with('error', 'Image upload failed.');
    }

    public function delete($id)
    {
        $data = Custom::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('success', 'Data berhasil dihapus.');
    }

}
