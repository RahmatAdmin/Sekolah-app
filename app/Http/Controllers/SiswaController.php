<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SiswaController extends Controller
{
    public function siswa()
    {
        return view ('students.siswa');
    }

    public function index()
    {
        // $dataframe = Siswa::where('is_accepted_phase_1', 1)->get();
        $dataframe = Siswa::all();
        return view('students.profile', compact('dataframe'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required|string|max:255',
            'jenis_kelamin' => 'required|string|max:255',
            'tanggal_lahir' => 'required|date|max:255',
            'alamat' => 'required|string|max:225',
            'skl' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
            'kk' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
            'akte' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
            'pas_foto' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
        ]);
        // $request->validate([
        //     'nama_lengkap' => 'required|string|max:255',
        //     'jenis_kelamin' => 'required|string|max:255',
        //     'tanggal_lahir' => 'required|date|max:255',
        //     'alamat' => 'required|string|max:225',
        //     'skl' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
        //     'kk' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
        //     'akte' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
        //     'pas_foto' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', 
        // ]);

        if ($validator->fails()) {
            Log::info('pendaftaran siswa ' . $request->nama_lengkap . ' dari dashboard | gagal' . $validator->errors());
            return redirect('siswa')->withErrors("input yang anda masukan salah");
        }

        $nama_gambar_1 = time() . '1.' . $request->skl->getClientOriginalExtension();
        $nama_gambar_2 = time() . '2.' . $request->kk->getClientOriginalExtension();
        $nama_gambar_3 = time() . '3.' . $request->akte->getClientOriginalExtension();
        $nama_gambar_4 = time() . '4.' . $request->pas_foto->getClientOriginalExtension();

        Log::info('pendaftaran siswa ' . $request->nama_lengkap . ' dari dashboard | sampai validasi');
        
        $request->skl->storeAs('public/', $nama_gambar_1);
        $request->kk->storeAs('public/', $nama_gambar_2);
        $request->akte->storeAs('public/', $nama_gambar_3);
        $request->pas_foto->storeAs('public/', $nama_gambar_4);

        Log::info('pendaftaran siswa ' . $request->nama_lengkap . ' dari dashboard | sampai store');

        try {
            $siswa = Siswa::create([
                'nama_lengkap' => $request->nama_lengkap,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tanggal_lahir' => $request->tanggal_lahir,
                'alamat' => $request->alamat,
                'skl' => $nama_gambar_1,
                'kk' => $nama_gambar_2,
                'akte' => $nama_gambar_3, 
                'pas_foto' => $nama_gambar_4,
            ]);
            Log::info('pendaftaran siswa ' . $siswa->id . ' success | nama ' . $siswa->nama_lengkap);
        } catch (\Exception $e) {
            Log::error('pendaftaran siswa failed | error : ' . $e->getMessage());
            return redirect('siswa')->with('error', 'Data siswa gagal disimpan.');
        }

        return redirect('siswa')->with('success', 'Data siswa berhasil disimpan.');
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');

        $dataframe = Siswa::where('nama_lengkap', 'like', '%' . $keyword . '%')
            ->orWhere('jenis_kelamin', 'like', '%' . $keyword . '%')
            ->orWhere('tanggal_lahir', 'like', '%' . $keyword . '%')
            ->orWhere('alamat', 'like', '%' . $keyword . '%')
            ->orWhere('akte', 'like', '%' . $keyword . '%')
            ->orWhere('kk', 'like', '%' . $keyword . '%')
            ->orWhere('skl', 'like', '%' . $keyword . '%')
            ->orWhere('pas_foto', 'like', '%' . $keyword . '%')
            ->get();

        return view('students.profile', ['dataframe' => $dataframe]);
    }


    public function destroy($id)
    {
        $siswa = Siswa::find($id);
        
        if (!$siswa) {
            return redirect()->route('dashboard.index')->with('error', 'Data siswa tidak ditemukan');
        }
        $siswa->delete();
        return redirect()->route('dashboard.index')->with('success', 'Data siswa berhasil dihapus');
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('students.edit', compact('siswa'));
    }


    public function update($id, Request $request)
    {
        $siswa = Siswa::findOrFail($id);

        $request->validate([
            'nama_lengkap' => 'required|string|max:255',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'nullable|string',
        ]);

        $siswa->update([
            'nama_lengkap' => $request->nama_lengkap,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
        ]);

        return redirect()->route('dashboard.index')->with('success', 'Data Siswa berhasil diperbarui');
    }
}
