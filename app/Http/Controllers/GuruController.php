<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index()
    {
        $dataguru = Guru::paginate(15);
        return view('teachers.pegawai', compact('dataguru'));
    }

    public function store(Request $request)
{
    $request->validate([
        'name' => 'required|string|max:255',
        'nip' => 'required|string|max:255',
        'email' => 'required|string|max:255',
        'birthdate' => 'required|date',
        'wa' => 'required|string|max:255',
        'gender' => 'required|string|max:255',
        'kualifikasi' => 'required|string|max:255',
        'pas_foto' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
    ]);

    $data = $request->all();

    if ($request->hasFile('pas_foto')) {
        $file = $request->file('pas_foto');
        $filename = time() . '_' . $file->getClientOriginalName();
        $path = $file->storeAs('pas_foto', $filename, 'public');
        $data['pas_foto'] = $path;
    }

    Guru::create($data);

    return redirect()->route('guru.index')->with('success', 'Data guru berhasil disimpan.');
}


    public function destroy($id)
    {
        $guru = Guru::find($id);

        if (!$guru) {
            return redirect()->route('dashboard.index')->with('error', 'Data guru$guru tidak ditemukan');
        }

        $guru->delete();

        return redirect()->route('guru.index')->with('success', 'Data guru berhasil dihapus');
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');

        $dataguru = Guru::where('name', 'like', '%' . $keyword . '%')
            //   ->orWhere('nisn', 'like', '%'.$keyword.'%')
            ->orWhere('nip', 'like', '%' . $keyword . '%')
            ->orWhere('email', 'like', '%' . $keyword . '%')
            ->paginate(15);

        return view('teachers.pegawai', ['dataguru' => $dataguru]);
    }

    public function edit($id)
    {
        $guru = Guru::findOrFail($id);

        // dd($guru);
        return view('teachers.edit', compact('guru'));
    }

    public function update($id, Request $request)
    {
        $guru = Guru::findOrFail($id);

        $request->validate([
            'name' => 'required|string|max:255',
            'nip' => 'required|string|max:255',
            'email' => 'nullable|string|max:255',
            'wa' => 'nullable|string|max:255',
            'address' => 'nullable|string|max:255',
            'gender' => 'nullable|string|max:255',
            'kualifikasi' => 'nullable|string|max:255',
        ]);

        $guru->update($request->all());

        return redirect()->route('guru.index')->with('success', 'Data guru berhasil diperbarui');
    }
}
