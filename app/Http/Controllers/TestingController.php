<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TestingController extends Controller
{
    //
    public function testing_upload(Request $req)
    {
        $method = $req->method();


        if ($method == 'POST') {
            // $image = $req->file('pas_foto');
            // dd($image);
            $validator = Validator::make($req->all(), [
                'pas_foto' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'akte_lahir' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'ijazah_skl' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                'kartu_keluarga' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);

            if ($validator->fails() && $method == 'POST') {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            dd($req->all());
        }

        return view('frontend.testing', ['value' => ['nisn' => $req->input('nisn') ?? null], 'btn' => ['title' => 'Testing Submit', 'url' => '/testing']]);
    }
}
