<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Custom;
use App\Models\Orangtua;
use App\Models\Siswa;
use App\Services\WhatsAppService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function show()
    {
        $data = Custom::all();
        $allartikel = Artikel::all();
        return view('frontend.website.index', compact('data', 'allartikel'));
    }

    public function info()
    {
        return view('frontend.informasi');
    }

    public function artikel()
    {
        return view('frontend.website.index');
    }

    public function detail()
    {
        return view('frontend.detail');
    }

    public function daftar(Request $req, WhatsAppService $waService)
    {
        $method = $req->method();
        $data = [
            'step' => 1,
            'display' => [
                'pribadi' => 'display: block;',
                'ayah' => 'display: block;',
                'ibu' => 'display: block;',
                'berkas' => 'display: block;',
                'registrasi' => 'display: block;'
            ],
            'value' => [
                'pribadi' => [
                    'namaLengkap' => $req->old('nama-lengkap'),
                    'namaLaqob' => $req->old('nama-laqob'),
                    'gender' => $req->old('gender'),
                    'asalSekolah' => $req->old('asal-sekolah'),
                    'tgl-lahir' => $req->old('tgl-lahir'),
                    'no-tlp' => $req->old('no-tlp'),
                    'alamat' => $req->old('alamat'),
                ],
                'ayah' => [
                    'namaLengkap' => $req->old('nama-lengkap-ayah'),
                    'tgl-lahir-ayah' => $req->old('tgl-lahir-ayah'),
                    'alamat' => $req->old('alamat-ayah'),
                    'agama' => $req->old('agama-ayah'),
                    'pendidikan' => $req->old('pendidikan-ayah'),
                    'pekerjaan-ayah' => $req->old('pekerjaan-ayah'),
                    'penghasilan' => $req->old('penghasilan-ayah'),
                    'no-telpon-ayah' => $req->old('no-telpon-ayah')
                ],
                'ibu' => [
                    'namaLengkap' => $req->old('nama-lengkap-ibu'),
                    'tgl-lahir-ibu' => $req->old('tgl-lahir-ibu'),
                    'alamat' => $req->old('alamat-ibu'),
                    'agama' => $req->old('agama-ibu'),
                    'pendidikan' => $req->old('pendidikan-ibu'),
                    'pekerjaan' => $req->old('pekerjaan-ibu'),
                    'penghasilan' => $req->old('penghasilan-ibu'),
                    'no-telpon-ibu' => $req->old('no-telpon-ibu')
                ],
                'berkas' => [
                    // 'akte_lahir' => null,
                    // 'kartu_keluarga' => null,
                    // 'ijazah_skl' => null,
                    // 'pas_foto' => null,
                    'nisn' => $req->old('nisn')
                ],
                'registrasi' => [
                    'email' => $req->old('email'),
                    'password' => $req->old('password'),
                ]
            ],
            'btn' => [
                'url' => '',
                'title' => 'Submit'
            ]
        ];

        if ($method == 'POST') {
            // validator
            // variable for debunging
            $berkas_debug = [];

            if (!$req->isJson()) {
                $berkas_debug = [
                    'akte_lahir' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'kartu_keluarga' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'ijazah_skl' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'pas_foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ];
            }
            $validator = Validator::make($req->all(), [
                'nama-lengkap' => 'required|max:255|min:5',
                'tgl-lahir' => 'required|max:255|min:5',
                'nama-laqob' => 'max:255',
                'no-tlp' => 'required|numeric|starts_with:62|min:5',
                'gender' => 'required',
                'asal-sekolah' => 'required|max:255|min:5',
                'alamat' => 'required|max:255|min:5',
                // ayah
                'nama-lengkap-ayah' => 'required|max:255|min:5',
                'tgl-lahir-ayah' => 'required|max:255|min:5',
                'alamat-ayah' => 'required|min:5',
                'agama-ayah' => 'required',
                'pendidikan-ayah' => 'required',
                'pekerjaan-ayah' => 'required',
                'penghasilan-ayah' => 'required',
                'no-telpon-ayah' => 'required|numeric|starts_with:62|min:5',
                // ibu
                'nama-lengkap-ibu' => 'required|max:255|min:5',
                'tgl-lahir-ibu' => 'required|max:255|min:5',
                'alamat-ibu' => 'required|min:5',
                'agama-ibu' => 'required',
                'pendidikan-ibu' => 'required',
                'pekerjaan-ibu' => 'required',
                'penghasilan-ibu' => 'required',
                'no-telpon-ibu' => 'required|numeric|starts_with:62|min:5',
                // berkas
                'nisn' => 'required|numeric',
                ...$berkas_debug
            ], [
                'required' => ':attribute harus diisi.',
                'starts_with' => ':attribute harus diawali dengan 62.',
                'email' => ':attribute harus diisi dengan email.',
                'numeric' => ':attribute harus diisi dengan angka.',
                'image' => ':attribute harus berupa gambar.',
            ], [
                'email' => 'Email',
                'password' => 'Password',
                'nisn' => 'NISN',
                'gender' => 'Jenis Kelamin',
                'asal-sekolah' => 'Asal Sekolah',
                'tgl-lahir' => 'Tanggal Lahir',
                'no-tlp' => 'Nomor Telepon',
                'alamat' => 'Alamat',
                'nama-lengkap' => 'Nama Lengkap',
                'nama-laqob' => 'Nama Laqob',
                'agama-ayah' => 'Agama Ayah',
                'pendidikan-ayah' => 'Pendidikan Ayah',
                'pekerjaan-ayah' => 'Pekerjaan Ayah',
                'penghasilan-ayah' => 'Penghasilan Ayah',
                'no-telpon-ayah' => 'Nomor Telepon Ayah',
                'agama-ibu' => 'Agama Ibu',
                'pendidikan-ibu' => 'Pendidikan Ibu',
                'pekerjaan-ibu' => 'Pekerjaan Ibu',
                'penghasilan-ibu' => 'penghasilan Ibu',
                'no-telpon-ibu' => 'Nomor Telepon Ibu',
                'nama-lengkap-ayah' => 'Nama lengkap Ayah',
                'tgl-lahir-ayah' => 'Tanggal Lahir Ayah',
                'alamat-ayah' => 'Alamat Ayah',
                'nama-lengkap-ibu' => 'Nama lengkap Ibu',
                'tgl-lahir-ibu' => 'Tanggal Lahir Ibu',
                'alamat-ibu' => 'Alamat Ibu',
                'pas_foto' => 'Pas Foto',
                'akte_lahir' => 'Akte Lahir',
                'kartu_keluarga' => 'Kartu Keluarga',
                'ijazah_skl' => 'Ijazah Sekolah',
            ]);

            if ($validator->fails()) {
                Log::error($validator->errors());
                if ($req->isJson()) {
                    return response()->json($validator->errors(), 422);
                } else {
                    return Redirect::back()->withErrors($validator)->withInput();
                }
            }
            DB::beginTransaction();
            try {
                $new_siswa_berkas = [];
                if (!$req->isJson()) {
                    $file_akte = $req->file('akte_lahir');
                    $file_kartu = $req->file('kartu_keluarga');
                    $file_ijazah = $req->file('ijazah_skl');
                    $file_pas = $req->file('pas_foto');
                    $fileName_akte = time() . '_berkas.' . $file_akte->getClientOriginalExtension();
                    $fileName_kartu = time() . '_berkas.' . $file_kartu->getClientOriginalExtension();
                    $fileName_ijazah = time() . '_berkas.' . $file_ijazah->getClientOriginalExtension();
                    $fileName_pas = time() . '_berkas.' . $file_pas->getClientOriginalExtension();

                    $file_akte->storeAs('public/', $fileName_akte);
                    $file_kartu->storeAs('public/', $fileName_kartu);
                    $file_ijazah->storeAs('public/', $fileName_ijazah);
                    $file_pas->storeAs('public/', $fileName_pas);
                    $new_siswa_berkas = [
                        'skl' => $fileName_ijazah,
                        'pas_foto' => $fileName_pas,
                        'akte' => $fileName_akte,
                        'kk' => $fileName_kartu,
                    ];
                }


                $new_siswa = Siswa::create([
                    'nama_lengkap' => $req->input('nama-lengkap'),
                    'nama_laqob' => $req->input('nama-laqob'),
                    'asal_sekolah' => $req->input('asal-sekolah'),
                    'no_telp' => $req->input('no-tlp'),
                    'jenis_kelamin' => $req->input('gender'),
                    'tanggal_lahir' => $req->input('tgl-lahir'),
                    'alamat' => $req->input('alamat'),
                    'nisn' => $req->input('nisn'),
                    ...$new_siswa_berkas
                ]);
                $new_ayah = Orangtua::create([
                    'full_name' => $req->input('nama-lengkap-ayah'),
                    'address' => $req->input('alamat-ayah'),
                    'birth_date' => $req->input('tgl-lahir-ayah'),
                    'religion' => $req->input('agama-ayah'),
                    'last_education' => $req->input('pendidikan-ayah'),
                    'job' => $req->input('pekerjaan-ayah'),
                    'phone' => $req->input('no-telpon-ayah'),
                    'income' => $req->input('penghasilan-ayah'),
                    'gender' => "Laki-laki",
                    'siswa_id' => $new_siswa->id,
                ]);
                $new_ibu = Orangtua::create([
                    'full_name' => $req->input('nama-lengkap-ibu'),
                    'address' => $req->input('alamat-ibu'),
                    'birth_date' => $req->input('tgl-lahir-ibu'),
                    'religion' => $req->input('agama-ibu'),
                    'last_education' => $req->input('pendidikan-ibu'),
                    'job' => $req->input('pekerjaan-ibu'),
                    'phone' => $req->input('no-telpon-ibu'),
                    'income' => $req->input('penghasilan-ibu'),
                    'gender' => "Perempuan",
                    'siswa_id' => $new_siswa->id,
                ]);
                DB::commit();
                // return view()
                Log::info('pendaftaran siswa ' . $new_siswa->id . ' success | nama ' . $new_siswa->nama_lengkap);
                $wa_service = $waService->send_verif($new_siswa->id);
                if ($req->isJson()) {
                    return response()->json(['success' => true , 'request' => $req->all(), 'data' => ['siswa' => $new_siswa, 'ayah' => $new_ayah, 'ibu' => $new_ibu] , 'wa' => $wa_service]);
                } else {
                    return view('frontend.Elements.pendaftaran.success', $data);
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                DB::rollBack();
                if ($req->isJson()) {
                    return response()->json(['success' => false,'request' => $req->all(), 'error' => $e->getMessage()]);
                } else {
                    return view('frontend.Elements.pendaftaran.error');
                }
            }
        }
        return view('frontend.pendaftaran', $data);
    }

    public function daftar_back(Request $req)
    {
        $step = $req->query('step', '1');
        $method = $req->method();

        // start variable for validation
        $rules_default = [
            'nama-lengkap' => 'required|max:255|min:5',
            'tgl-lahir' => 'required|max:255|min:5',
            'no-tlp' => 'required|numeric|starts_with:62|min:5',
        ];
        $messages_default = [
            'required' => ':attribute harus diisi.',
            'max' => ':attribute maksimal :max karakter.',
            'min' => ':attribute minimal :min karakter.',
            'starts_with' => ':attribute harus diawali dengan 62.',
            'numeric' => ':attribute harus diisi dengan angka.',
        ];
        $attributes_default = [
            'nama-lengkap' => 'Nama Lengkap',
            'tgl-lahir' => 'Tanggal Lahir',
            'no-tlp' => 'Nomor Telepon',
        ];
        // end of variable for validation

        // start of dummy data for frontend blade
        $data = [
            'title' => 'Step 1 - Personal',
            'display' => [
                'pribadi' => 'display: none;',
                'ayah' => 'display: none;',
                'ibu' => 'display: none;',
                'berkas' => 'display: none;',
                'registrasi' => 'display: none;'
            ],
            'value' => [
                'pribadi' => [
                    'namaLengkap' => $req->input('nama-lengkap') ?? $req->old('nama-lengkap'),
                    'namaLaqob' => $req->input('nama-laqob') ?? $req->old('nama-laqob'),
                    'gender' => $req->input('gender') ?? $req->old('gender'),
                    'asalSekolah' => $req->input('asal-sekolah') ?? $req->old('asal-sekolah'),
                    'tglLahir' => $req->input('tgl-lahir') ?? $req->old('tgl-lahir'),
                    'noTlp' => $req->input('no-tlp') ?? $req->old('no-tlp'),
                    'alamat' => $req->input('alamat') ?? $req->old('alamat'),
                ],
                'ayah' => [
                    'namaLengkap' => $req->input('nama-lengkap-ayah') ?? $req->old('nama-lengkap-ayah'),
                    'tglLahir' => $req->input('tgl-lahir-ayah') ?? $req->old('tgl-lahir-ayah'),
                    'alamat' => $req->input('alamat-ayah') ?? $req->old('alamat-ayah'),
                    'agama' => $req->input('agama-ayah') ?? $req->old('agama-ayah'),
                    'pendidikan' => $req->input('pendidikan-ayah') ?? $req->old('pendidikan-ayah'),
                    'pekerjaan' => $req->input('pekerjaan-ayah') ?? $req->old('pekerjaan-ayah'),
                    'penghasilan' => $req->input('penghasilan-ayah') ?? 0,
                    'noTlp' => $req->input('no-telpon-ayah') ?? $req->old('no-telpon-ayah')
                ],
                'ibu' => [
                    'namaLengkap' => $req->input('nama-lengkap-ibu') ?? $req->old('nama-lengkap-ibu'),
                    'tglLahir' => $req->input('tgl-lahir-ibu') ?? $req->old('tgl-lahir-ibu'),
                    'alamat' => $req->input('alamat-ibu') ?? $req->old('alamat-ibu'),
                    'agama' => $req->input('agama-ibu') ?? $req->old('agama-ibu'),
                    'pendidikan' => $req->input('pendidikan-ibu') ?? $req->old('pendidikan-ibu'),
                    'pekerjaan' => $req->input('pekerjaan-ibu') ?? $req->old('pekerjaan-ibu'),
                    'penghasilan' => $req->input('penghasilan-ibu') ?? $req->old('penghasilan-ibu'),
                    'noTlp' => $req->input('no-telpon-ibu') ?? $req->old('no-telpon-ibu')
                ],
                'berkas' => [
                    // 'akte_lahir' => null,
                    // 'kartu_keluarga' => null,
                    // 'ijazah_skl' => null,
                    // 'pas_foto' => null,
                    'nisn' => $req->input('nisn') ?? $req->old('nisn')
                ],
                'registrasi' => [
                    'email' => $req->input('email') ?? $req->old('email'),
                    'password' => $req->input('password') ?? $req->old('password'),
                ]
            ],
            'btn' => [
                'url' => '?step=' . $step,
                'title' => 'Selanjutnya'
            ]
        ];
        // end of dummy data for frontend blade


        if ($step == '1') {
            // updating dynamic data
            $data['title'] = 'Step 1 - Personal';
            $data['display']['pribadi'] = 'display: block;';
            $data['btn']['url'] = '?step=2';
            $data['btn']['title'] = 'Selanjutnya';
            $data['step'] = 1;
            // end of updating dynamic data
        } elseif ($step == '2') {
            // validator for step 1
            if ($method == 'POST') {
                // starting validation data request STEP 1
                $rules = [
                    ...$rules_default,
                    'nama-laqob' => 'required|max:100|min:3',
                    'gender' => 'required|in:Laki-laki,Perempuan|min:1',
                    'asal-sekolah' => 'required|max:255|min:5',
                    'alamat' => 'required|max:255|min:5',
                    'tgl-lahir' => 'required|date'
                ];
                $attributes = [
                    ...$attributes_default,
                    'nama-laqob' => 'Nama Laqob',
                    'gender' => 'Jenis Kelamin',
                    'asal-sekolah' => 'Asal Sekolah',
                    'tgl-lahir' => 'Tanggal Lahir',
                    'no-tlp' => 'Nomor Telepon',
                ];
                $messages = [
                    ...$messages_default,
                    'required' => ':attribute harus diisi.',
                    'max' => ':attribute maksimal :max karakter.',
                    'min' => ':attribute minimal :min karakter.',
                    'starts_with' => ':attribute harus diawali dengan 62.',
                    'in' => ':attribute harus diisi dengan L atau P.',
                    'numeric' => ':attribute harus diisi dengan angka.',
                ];
                $validator = Validator::make($req->all(), $rules, $messages, $attributes);

                if ($validator->fails() && $method == 'POST') {
                    // log("validation error: " . json_encode($validator->errors()));
                    return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
                }
                // end of validation data request
            }
            $data['title'] = 'Step 2 - Ayah';
            $data['display']['ayah'] = 'display: block;';
            $data['btn']['url'] = '?step=3';
            $data['btn']['title'] = 'Selanjutnya';
            $data['step'] = 2;
        } elseif ($step == '3') {
            // VALIDATOR STEP 2
            if ($method == 'POST') {
                // starting validation data request STEP 2
                $rules = [
                    ...$rules_default,
                    "nama-lengkap-ayah" => "required|max:255|min:5",
                    "tgl-lahir-ayah" => "required|max:255|min:5",
                    "no-telpon-ayah" => "required|numeric|starts_with:62|min:5",
                    "pendidikan-ayah" => "required|max:255",
                    "pekerjaan-ayah" => "required|max:255",
                    "penghasilan-ayah" => "required|max:255|min:5",
                ];
                $attributes = [
                    ...$attributes_default,
                    "nama-lengkap-ayah" => "Nama Lengkap Ayah",
                    "tgl-lahir-ayah" => "Tanggal Lahir Ayah",
                    "no-telpon-ayah" => "Nomor Telepon Ayah",
                    "pendidikan-ayah" => "Pendidikan Ayah",
                    "pekerjaan-ayah" => "Pekerjaan Ayah",
                    "penghasilan-ayah" => "Penghasilan Ayah",
                ];
                $messages = [
                    ...$messages_default,
                    "starts_with" => ":attribute harus diawali dengan 62.",
                    "numeric" => ":attribute harus diisi dengan angka.",
                    "min" => ":attribute minimal :min karakter.",
                ];

                $validator = Validator::make($req->all(), $rules, $messages, $attributes);

                if ($validator->fails() && $method == 'POST') {
                    Log::error("validation error: " . json_encode($validator->errors()));
                    return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
                }
                // end of validation data request
            }
            $data['title'] = 'Step 3 - Ibu';
            $data['display']['ibu'] = 'display: block;';
            $data['btn']['url'] = '?step=4';
            $data['btn']['title'] = 'Selanjutnya';
            $data['step'] = 3;
        } elseif ($step == '4') {
            // VALIDATOR STEP 3
            if ($method == 'POST') {
                // starting validation data request STEP 3
                $rules = [
                    ...$rules_default,
                    "nama-lengkap-ibu" => "required|max:255|min:5",
                    "tgl-lahir-ibu" => "required|max:255|min:5",
                    "no-telpon-ibu" => "required|numeric|starts_with:62|min:5",
                    "pendidikan-ibu" => "required|max:255",
                    "pekerjaan-ibu" => "required|max:255",
                    "penghasilan-ibu" => "required|max:255|min:5",
                ];
                $attributes = [
                    ...$attributes_default,
                    "nama-lengkap-ibu" => "Nama Lengkap Ibu",
                    "tgl-lahir-ibu" => "Tanggal Lahir Ibu",
                    "no-telpon-ibu" => "Nomor Telepon Ibu",
                    "pendidikan-ibu" => "Pendidikan Ibu",
                    "pekerjaan-ibu" => "Pekerjaan Ibu",
                    "penghasilan-ibu" => "Penghasilan Ibu",
                ];
                $messages = [
                    ...$messages_default,
                    "starts_with" => ":attribute harus diawali dengan 62.",
                    "numeric" => ":attribute harus diisi dengan angka.",
                    "min" => ":attribute minimal :min karakter.",
                ];

                $validator = Validator::make($req->all(), $rules, $messages, $attributes);

                if ($validator->fails() && $method == 'POST') {
                    return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            $data['title'] = 'Step 4 - Registrasi Akun Pendaftaran';
            $data['display']['registrasi'] = 'display: block;';
            $data['btn']['url'] = '?step=5';
            $data['btn']['title'] = 'Selanjutnya';
            $data['step'] = 4;
        } elseif ($step == '5') {
            // VALIDATOR STEP 4
            if ($method == 'POST') {
                // starting validation data request STEP 4
                $rules = [
                    ...$rules_default,
                    "email" => "required|unique:users|email|max:255|min:5",
                    "password" => "required|max:255|min:5",
                ];
                $attributes = [
                    ...$attributes_default,
                    "email" => "Email",
                    "password" => "Password",
                ];
                $messages = [
                    ...$messages_default,
                    "email" => "Format harus berupa :attribute yang valid.",
                    "unique" => "Email :attribute sudah terdaftar.",
                ];
            }
            $data['title'] = 'Step 5 - Berkas';
            $data['display']['berkas'] = 'display: block;';
            $data['btn']['url'] = '?step=6';
            $data['btn']['title'] = 'Submit';
            $data['step'] = 5;
        } elseif ($step == '6') {
            // VALIDATOR STEP 5
            if ($method == 'POST') {
                // starting validation data request STEP 5
                $rules = [
                    "pas_foto" => "required|image|mimes:jpeg,png,jpg,gif|max:2048",
                    "akte_lahir" => "required|image|mimes:jpeg,png,jpg,gif|max:2048",
                    "ijazah_skl" => "required|image|mimes:jpeg,png,jpg,gif|max:2048",
                    "kartu_keluarga" => "required|image|mimes:jpeg,png,jpg,gif|max:2048",
                    "nisn" => "required|numeric|max:255|min:5",
                ];
                $attributes = [
                    "pas_foto" => "Pas Foto",
                    "akte_lahir" => "Akte Lahir",
                    "ijazah_skl" => "Ijazah Sekolah",
                    "kartu_keluarga" => "Kartu Keluarga",
                    "nisn" => "NISN",
                ];
                $messages = [
                    "required" => ":attribute harus diisi.",
                    "image" => ":attribute harus berupa gambar.",
                    "mimes" => ":attribute harus berupa :mimes.",
                    "max" => ":attribute maksimal :max kilobytes.",
                ];

                $validator = Validator::make($req->all(), $rules, $messages, $attributes);

                if ($validator->fails() && $method == 'POST') {
                    return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
                }
            }
            try {
                // dd($req);
                $file_akte = $req->file('akte_lahir');
                $file_kartu = $req->file('kartu_keluarga');
                $file_ijazah = $req->file('ijazah_skl');
                $file_pas = $req->file('pas_foto');
                $fileName_akte = time() . '.' . $file_akte->getClientOriginalExtension();
                $fileName_kartu = time() . '.' . $file_kartu->getClientOriginalExtension();
                $fileName_ijazah = time() . '.' . $file_ijazah->getClientOriginalExtension();
                $fileName_pas = time() . '.' . $file_pas->getClientOriginalExtension();

                $file_akte->storeAs('public/berkas', $fileName_akte);
                $file_kartu->storeAs('public/berkas', $fileName_kartu);
                $file_ijazah->storeAs('public/berkas', $fileName_ijazah);
                $file_pas->storeAs('public/berkas', $fileName_pas);

                DB::beginTransaction();
                // $new_user = User::create([
                //     'name' => $data['value']['pribadi']['namaLengkap'],
                //     'email' => $data['value']['registrasi']['email'],
                //     'password' => bcrypt($data['value']['registrasi']['password']),
                // ]);
                $new_siswa = Siswa::create([
                    'nama_lengkap' => $data['value']['pribadi']['namaLengkap'],
                    'nama_laqob' => $data['value']['pribadi']['namaLaqob'],
                    'asal_sekolah' => $data['value']['pribadi']['asalSekolah'],
                    'no_telp' => $data['value']['pribadi']['noTlp'],
                    'jenis_kelamin' => $data['value']['pribadi']['gender'],
                    'tanggal_lahir' => $data['value']['pribadi']['tglLahir'],
                    'alamat' => $data['value']['pribadi']['alamat'],
                    'nisn' => $data['value']['berkas']['nisn'],
                    'skl' => 'berkas/' . $fileName_ijazah,
                    'pas_foto' => 'berkas/' . $fileName_pas,
                    'akte' => 'berkas/' . $fileName_akte,
                    'kk' => 'berkas/' . $fileName_kartu,
                ]);
                $new_ayah = Orangtua::create([
                    'full_name' => $data['value']['ayah']['namaLengkap'],
                    'address' => $data['value']['ayah']['alamat'],
                    'birth_date' => $data['value']['ayah']['tglLahir'],
                    'religion' => $data['value']['ayah']['agama'],
                    'last_education' => $data['value']['ayah']['pendidikan'],
                    'job' => $data['value']['ayah']['pekerjaan'],
                    'phone' => $data['value']['ayah']['noTlp'],
                    'income' => $data['value']['ayah']['penghasilan'],
                    'gender' => "Laki-laki",
                    'siswa_id' => $new_siswa->id,
                ]);
                $new_ibu = Orangtua::create([
                    'full_name' => $data['value']['ibu']['namaLengkap'],
                    'address' => $data['value']['ibu']['alamat'],
                    'birth_date' => $data['value']['ibu']['tglLahir'],
                    'religion' => $data['value']['ibu']['agama'],
                    'last_education' => $data['value']['ibu']['pendidikan'],
                    'job' => $data['value']['ibu']['pekerjaan'],
                    'phone' => $data['value']['ibu']['noTlp'],
                    'income' => $data['value']['ibu']['penghasilan'],
                    'gender' => "Perempuan",
                    'siswa_id' => $new_siswa->id,
                ]);
                // $new_user_login_detail = UserLoginDetail::create([
                //     'user_id' => $new_user->id,
                //     'siswa_id' => $new_siswa->id,
                //     'role_id' => 2
                // ]);
                DB::commit();
                return view('frontend.Elements.pendaftaran.success', $data);
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error($e->getMessage());
                return view('frontend.Elements.pendaftaran.error');
            }
            // dd($data);
        }


        return view('frontend.pendaftaran', $data);
    }
}
