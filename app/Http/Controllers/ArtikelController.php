<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    public function show()
    {
        $articleframe = Artikel::all();
        return view('articles.artikel', compact('articleframe'));
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tittle' => 'required|string|max:255',
            'desc' => 'required|string',
            'content' => 'required|string',
            'img' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'date' => 'required|date',
        ]);

        $imagePath = time() . '1.' . $request->img->getClientOriginalExtension();

        $request->img->storeAs('/public', $imagePath);
        
        $artikel = Artikel::create([
            'tittle' => $request->tittle,
            'desc' => $request->desc,
            'content' => $request->content,
            'img' => $imagePath, // Menggunakan $imagePath yang sudah dibuat
            'date' => $request->date,
        ]);
        

        return redirect()->route('dashboard.artikel', $artikel->id)
            ->with('success', 'Artikel berhasil ditambahkan.');
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
    
        $articleframe = Artikel::where('tittle', 'like', "%$keyword%")
                                ->orWhere('desc', 'like', "%$keyword%")
                                ->orWhere('content', 'like', "%$keyword%")
                                ->orWhere('date', 'like', "%$keyword%")
                                ->get();
    
        return view('articles.artikel', compact('articleframe', 'keyword'));
    }

    
    public function edit($id)
    {
        $artikel = Artikel::findOrFail($id);
        return view('articles.edit', compact('artikel'));
    }

    public function update($id, Request $request)
{
    $artikel = Artikel::findOrFail($id);

    $request->validate([
        'tittle' => 'required|string|max:255',
        'content' => 'required',
        'desc' => 'required',
        'date' => 'required|date',
    ]);

    $artikel-> update([
        'tittle' => $request->tittle,
        'content' => $request->content,
        'desc' => $request->desc,
        'date' => $request->date,
    ]);


    return redirect()->route('dashboard.artikel')->with('success', 'Artikel berhasil diperbarui');
}


    public function destroy($id)
    {
        $artikel = Artikel::find($id);
        if (!$artikel) {
            return redirect()->route('dashboard.artikel')->with('error', 'Data Artikel tidak ditemukan');
        }
        $artikel->delete();
        return redirect()->route('dashboard.artikel')->with('success', 'Data Artikel berhasil dihapus');
    }

}
