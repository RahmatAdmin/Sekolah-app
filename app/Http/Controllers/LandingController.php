<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index() {
        $allartikel = Artikel::all();
        return view('frontend.artikel', compact('allartikel'));
    }

    public function show(Artikel $article)
    {
        return view('frontend.detail', compact('article'));
    }
    
}
