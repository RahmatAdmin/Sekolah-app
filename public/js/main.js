function openModal() {
    var modal = document.getElementById('myModal');
    modal.style.display = 'block';
    setTimeout(function () {
      modal.querySelector('.modal-content').classList.add('show');
    }, 50); // Munculkan setelah 50ms
  }

  // Fungsi untuk menutup modal
  function closeModal() {
    var modal = document.getElementById('myModal');
    modal.querySelector('.modal-content').classList.remove('show');
    setTimeout(function () {
      modal.style.display = 'none';
    }, 300); // Tutup setelah animasi selesai (300ms)
  }

  

   