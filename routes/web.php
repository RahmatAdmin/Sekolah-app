<?php

use App\Models\Siswa;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\CustomController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PendaftaranController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::get('/', [PageController::class, 'show']);

Route::get('/dashboard', function () {
    return view('admin.dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

// Dashboard
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/data-gedung', [DashboardController::class, 'build'])->name('dashboard.build');
Route::get('/informasi', [DashboardController::class, 'informasi'])->name('dashboard.informasi');


// landing page
Route::get('baitulQuran', [PageController::class,'show'])->name('page.head');
Route::get('/info', [PageController::class, 'info'])->name('page.info');
Route::get('/article', [PageController::class, 'artikel'])->name('page.artikel');
Route::get('/detail-artikel', [PageController::class, 'detail'])->name('page.detail');

Route::group(['prefix' => 'pendaftaran'], function () {
    Route::get('', [PageController::class, 'daftar'])->name('page.daftar');
    Route::post('', [PageController::class, 'daftar'])->name('page.daftar_post');
});


// siswa
Route::get('/siswa', [SiswaController::class, 'index'])->name('dashboard.index');
Route::post('/insert', [SiswaController::class, 'store'])->name('siswa.store');
Route::get('siswa/search', [SiswaController::class, 'search'])->name('siswa.search');
Route::delete('/siswa/{id}', [SiswaController::class, 'destroy'])->name('siswa.destroy');
Route::get('/siswa/{id}/edit', [SiswaController::class, 'edit'])->name('siswa.edit');
Route::put('/siswa/{id}', [SiswaController::class, 'update'])->name('siswa.update');

// guru
Route::get('/guru', [GuruController::class, 'index'])->name('guru.index');
Route::post('/guru/store', [GuruController::class, 'store'])->name('guru.store');
Route::get('/guru/{id}/edit', [GuruController::class, 'edit'])->name('guru.edit');
Route::put('/guru/{id}', [GuruController::class, 'update'])->name('guru.update');
Route::delete('/guru/{id}', [GuruController::class, 'destroy'])->name('guru.destroy');
Route::get('guru/search', [GuruController::class, 'search'])->name('guru.search');


// artikel
Route::get('/artikel', [ArtikelController::class, 'show'])->name('dashboard.artikel');
Route::post('/artikel/insert', [ArtikelController::class, 'store'])->name('artikel.store');
Route::get('/artikel/create', [ArtikelController::class, 'create'])->name('artikel.create');
Route::get('/articles/search', [ArtikelController::class, 'search'])->name('artikel.search');
Route::get('/artikel/{id}/edit', [ArtikelController::class, 'edit'])->name('artikel.edit');
Route::put('/artikel/{id}', [ArtikelController::class, 'update'])->name('artikel.update');
Route::delete('/artikel/{id}', [ArtikelController::class, 'destroy'])->name('artikel.destroy');


// list artikel
Route::get('/artikel-collection', [LandingController::class, 'index'])->name('artikel.collect');
Route::get('/articles/{article}', [LandingController::class, 'show'])->name('artikel.detail');


// Pendaftaran
Route::get('/data-pendaftar', [PendaftaranController::class, 'index'])->name('pendaftaran.index');
Route::post('/siswa/{id}/accept', [PendaftaranController::class, 'update'])->name('siswa.accept');


// custom
Route::get('/custom-layout', [CustomController::class, 'index'])->name('custom.index');
Route::post('/upload', [CustomController::class, 'upload'])->name('upload.store');
Route::delete('/delete/{id}', [CustomController::class, 'delete'])->name('delete.data');

// about
Route::get('/about', function() {
    return view('frontend.about');
});

Route::get('/bug', function() {
    return view('frontend.website.index');
})->name('page.index');

require __DIR__.'/auth.php';
