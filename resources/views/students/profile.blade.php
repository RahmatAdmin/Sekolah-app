@extends('layouts.layout')
<link rel="stylesheet" href="{{ asset('css/style.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous">
</script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">


@section('content')

@if(session('success'))
    <script>
        Swal.fire({
            title: 'success!',
            text: '{{ session('
            success ') }}',
            icon: 'success',
            customClass: {
                popup: 'swal2-popup'
            }
        });
    </script>
@endif



<section style="padding: 20px;">
    <div class="table-responsive table-responsive-xl" style="width: 100%; height: 800px;">
        <div class="row">
            <div style="width: 100%;">
                <div class="card">
                    <div style="background-color: #34495E" ; class="card-header text-white">
                        <h4 class="mb-0">Manage Data Siswa</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3">
                            <button style="background-color: #34495E" ; id="tambahDataBtn" class="btn text-white"
                                onclick="openModal()">Tambah Data</button>
                            <form action="{{ route('siswa.search') }}" method="GET" class="form-inline">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Cari..." name="keyword">
                                    <div class="input-group-append">
                                        <button style="background-color: #34495E" ; class="btn text-white"
                                            type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="table-responsive table-responsive-xl">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>TTL</th>
                                        <th>Alamat</th>
                                        <th>Skl</th>
                                        <th>Akte</th>
                                        <th>KK</th>
                                        <th>Pas Foto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataframe as $index => $siswa)
                                        <tr style="height: 80px;">
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $siswa->nama_lengkap }}</td>
                                            <td>{{ $siswa->tanggal_lahir }}</td>
                                            <td class="text-red">{{ $siswa->alamat }}</td>
                                            <td>
                                                @if($siswa->skl)
                                                    <!-- <a href="{{ asset('storage/' . $siswa->skl) }}" data-lightbox="skl" data-title="SKL" style="width: 100px; height: 100px;"> -->
                                                    <img src="{{ asset('storage/' . $siswa->skl) }}" alt="SKL"
                                                        style="width: 150px; height: 150px; border-radius: 0%;">
                                                @else
                                                    <p>Belum ada SKL diunggah.</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($siswa->akte)
                                                    <!-- <a href="{{ asset('storage/' . $siswa->akte) }}" data-lightbox="akte" data-title="akte"> -->
                                                    <img src="{{ asset('storage/' . $siswa->akte) }}" alt="Akte"
                                                        style="width: 150px; height: 150px; border-radius: 0%;">
                                                @else
                                                    <p>Belum ada Akte diunggah.</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($siswa->kk)
                                                    <!-- <a href="{{ asset('storage/' . $siswa->kk) }}" data-lightbox="kk" data-title="kk"> -->
                                                    <img src="{{ asset('storage/' . $siswa->kk) }}" alt="KK"
                                                        style="width: 150px; height: 150px; border-radius: 0%;">
                                                @else
                                                    <p>Belum ada KK diunggah.</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($siswa->pas_foto)
                                                    <!-- <a href="{{ asset('storage/' . $siswa->pas_foto) }}" data-lightbox="pas_foto" data-title="pas_foto"> -->
                                                    <img src="{{ asset('storage/' . $siswa->pas_foto) }}" alt="Pas Foto"
                                                        style="width: 150px; height: 150px; border-radius: 0%;">
                                                @else
                                                    <p>Belum ada Pas Foto diunggah.</p>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Aksi"
                                                    style="display: flex; gap: 10px;">
                                                    <button type="button" class="btn btn-primary">
                                                        <a href="{{ route('siswa.edit', $siswa->id) }}">
                                                            Edit
                                                        </a>
                                                    </button>
                                                    <button class="btn btn-danger" data-toggle="modal"
                                                        data-target="#confirmationModal{{ $siswa->id }}" data-id="{{ $siswa->id }}">
                                                            Hapus
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>


                                        <!-- Modal Konfirmasi Hapus -->
                                        <div class="modal fade" id="confirmationModal{{ $siswa->id }}" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger text-white">
                                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Hapus</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda yakin ingin menghapus data ini?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Cancel</button>
                                                        <form id="deleteForm" method="POST"
                                                            action="{{ route('siswa.destroy', $siswa->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger">Yes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div id="myModal" class="modal" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;">
        <div class="modal-content" style="animation: slideInFromTop 0.6s ease;">
            <div style="background-color: #34495E"; class="modal-header text-white">
                <h5 class="modal-title">Tambah Siswa</h5>
                <button type="button" class="close" onclick="closeModal()">
            <span>&times;</span>
    </button>
</div>

            <!-- Form add -->
            <div class="modal-body">
                <form action="{{ route('siswa.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="nama_lengkap" class="col-sm-3 col-form-label">Nama Lengkap:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin:</label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" required> -->
                             <select class="form-control" name="jenis_kelamin" required>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan" selected>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_lahir" class="col-sm-3 col-form-label">Tanggal Lahir:</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Alamat:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="alamat" name="alamat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">No Telp :</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nisn" name="nisn">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Nama Ayah:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama_ayah" name="nama_ayah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Nama Ibu:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nama_ibu" name="nama_ibu">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="skl" class="col-sm-3 col-form-label">SKL:</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" id="skl" name="skl" accept="image/*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="akte" class="col-sm-3 col-form-label">Akte:</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" id="akte" name="akte" accept="image/*"
                                required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kk" class="col-sm-3 col-form-label">KK:</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" id="kk" name="kk" accept="image/*" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pas_foto" class="col-sm-3 col-form-label">Pas Foto:</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control-file" id="pas_foto" name="pas_foto" accept="image/*"
                                required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-secondary" onclick="closeModal()">Batal</button>
                            <button style="background-color: #34495E"; type="submit" class="btn text-white">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous">
</script>

<script>
    function openModal() {
        $('#myModal').modal('show');
    }

    function closeModal() {
        $('#myModal').modal('hide');
    }
</script>
@endsection