
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>

<style>
    /* Desain modal */
    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.4);
    }

    /* Konten modal */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        border-radius: 10px;
        /* Tambahkan border radius */
    }

    /* Tombol penutup (x) */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    /* Tampilan tombol Simpan */
    .btn-submit {
        background-color: #4CAF50;
        color: white;
        padding: 10px 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
    }

    .modal-content {
        opacity: 0;
        transform: translateY(-50px);
        transition: opacity 0.3s ease, transform 0.3s ease;
    }

    .modal-content.show {
        opacity: 1;
        transform: translateY(0);
    }

    .btn-submit:hover {
        background-color: #45a049;
    }

    .success-alert {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 1000;
        background-color: #4CAF50;
        color: #FFF;
        border-radius: 0;
        padding: 15px 20px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        opacity: 0;
        animation: showSuccess 0.5s ease forwards, hideSuccess 4.5s ease forwards 0.5s;
        transition: height 0.9s ease, width 0.9s ease;
    }

    .success-alert i {
        margin-right: 10px;
        font-size: 20px;
    }

    .success-alert span {
        font-size: 16px;
    }

    @keyframes showSuccess {
        0% {
            opacity: 0;
            height: 0;
            width: 0;
        }

        100% {
            opacity: 1;
            height: auto;
            width: auto;
        }
    }

    @keyframes hideSuccess {
        0% {
            opacity: 1;
            height: auto;
            width: auto;
        }

        100% {
            opacity: 0;
            height: 0;
            width: 0;
        }
    }
    .modal-header .close {
        margin-left: auto;
        color: #000;
        font-size: 20px;
        background-color: transparent;
        border: none;
        padding: 0;
    }

    .modal-header .close:focus {
        outline: none;
    }
</style>

@if (session('success'))
<div class="container">
<div class="alert success-alert">
    <i class="fas fa-check-circle"></i>
    <span>
        {{ session('success') }}
        <i class="fas fa-check"></i>
    </span>
</div>
</div>
@endif



<div id="myModal" class="modal">
    <div class="modal-dialog modal-dialog-centered" style="max-width: 700px;">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title">Tambah Siswa</h5>
                <button type="button" class="close" onclick="closeModal()">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('siswa.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="nama_lengkap" class="col-sm-3 col-form-label">Nama Lengkap:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="tanggal_lahir" class="col-sm-3 col-form-label">Tanggal Lahir:</label>
        <div class="col-sm-9">
            <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required>
        </div>
    </div>
    <!-- New Fields -->
    <div class="form-group row">
        <label for="alamat" class="col-sm-3 col-form-label">Alamat:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="alamat" name="alamat">
        </div>
    </div>
    <div class="form-group row">
        <label for="skl" class="col-sm-3 col-form-label">SKL:</label>
        <div class="col-sm-9">
            <input type="file" class="form-control-file" id="skl" name="skl" accept="image/*" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="akte" class="col-sm-3 col-form-label">Akte:</label>
        <div class="col-sm-9">
            <input type="file" class="form-control-file" id="akte" name="akte" accept="image/*" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="kk" class="col-sm-3 col-form-label">KK:</label>
        <div class="col-sm-9">
            <input type="file" class="form-control-file" id="kk" name="kk" accept="image/*" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="pas_foto" class="col-sm-3 col-form-label">Pas Foto:</label>
        <div class="col-sm-9">
            <input type="file" class="form-control-file" id="pas_foto" name="pas_foto" accept="image/*" required>
        </div>
    </div>
    <!-- End of New Fields -->
    <div class="form-group row">
        <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-secondary" onclick="closeModal()">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
</form>
            </div>
        </div>
    </div>
</div>



<section>
    @foreach($dataframe as $siswa)
    <div style="display: flex; gap: 20px;">
        <div>{{ $siswa->nama_lengkap }}</div>
        <div>{{ $siswa->jenis_kelamin }}</div>
        <div>{{ $siswa->tanggal_lahir }}</div>
        <div>
            @if($siswa->skl)
            <!-- <a href="{{ asset('storage/' . $siswa->skl) }}" data-lightbox="skl" data-title="SKL" style="width: 100px; height: 100px;"> -->
            <img src="{{ asset('storage/' . $siswa->skl) }}" alt="SKL" style="width: 150px; height: 150px; border-radius: 50%;">
            @else
            <p>Belum ada SKL diunggah.</p>
            @endif
        </div>
        <div>
            @if($siswa->akte)
            <!-- <a href="{{ asset('storage/' . $siswa->akte) }}" data-lightbox="akte" data-title="akte"> -->
            <img src="{{ asset('storage/' . $siswa->akte) }}" alt="Akte" style="width: 150px; height: 150px; border-radius: 50%;">
            @else
            <p>Belum ada Akte diunggah.</p>
            @endif
        </div>
        <div>
            @if($siswa->kk)
            <!-- <a href="{{ asset('storage/' . $siswa->kk) }}" data-lightbox="kk" data-title="kk"> -->
            <img src="{{ asset('storage/' . $siswa->kk) }}" alt="KK" style="width: 150px; height: 150px; border-radius: 50%;">
            @else
            <p>Belum ada KK diunggah.</p>
            @endif
        </div>
        <div>
            @if($siswa->pas_foto)
            <!-- <a href="{{ asset('storage/' . $siswa->pas_foto) }}" data-lightbox="pas_foto" data-title="pas_foto"> -->
            <img src="{{ asset('storage/' . $siswa->pas_foto) }}" alt="Pas Foto" style="width: 150px; height: 150px; border-radius: 50%;">
            @else
            <p>Belum ada Pas Foto diunggah.</p>
            @endif
        </div>
    </div>
    @endforeach
</section>



<button onclick="openModal()">Tambah Data</button>
<script>
    window.addEventListener('load', function () {
        lightbox.init();
    });
</script>
<script src="{{ asset('js/main.js')}}"></script>