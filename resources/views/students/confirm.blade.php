<style>
    /* CSS untuk Modal */
.modal {
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0,0,0,0.4);
}

.modal-content {
    background-color: #fefefe;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 50%;
    text-align: center;
}

.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

/* CSS untuk pesan sukses */
.success-message {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #4CAF50;
    color: white;
    padding: 20px;
    border-radius: 5px;
    z-index: 999;
}

</style>


<!-- Modal Konfirmasi Hapus -->
<div id="confirmationModal" class="modal">
    <div class="modal-content">
        <span class="close" onclick="hideConfirmationModal()">&times;</span>
        <p style="color: black;">Apakah Anda yakin ingin menghapus data ini?</p>
        <button onclick="deleteStudent()">Hapus</button>
    </div>
</div>

<!-- Pesan Sukses -->
<div id="successMessage" class="success-message">
    <p>Data berhasil dihapus</p>
</div>
