@extends('layouts.layout')
<link rel="stylesheet" href="{{ asset('css/style.css')}}">

@section('content')
<!-- <section style="padding: 20px; background-color: #f4f6f9; border: none;"> -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        <h4 class="mb-0">Update Data Siswa</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('siswa.update', $siswa->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <section>
                                <div class="href-target" id="input-types"></div>
                                <div class="nice-form-group">
                                    <label>
                                        <i class="fas fa-user"></i> Nama Lengkap :
                                    </label>
                                    <input type="text" name="nama_lengkap" value="{{ $siswa->nama_lengkap }}" />
                                </div>

                                <div class="nice-form-group">
                                    <label>
                                        <i class="fas fa-venus-mars"></i> Jenis Kelamin :
                                    </label>
                                    <input type="date" name="tanggal_lahir" value="{{ $siswa->tanggal_lahir }}" />
                                </div>

                                <div class="nice-form-group">
                                    <label>
                                        <i class="fas fa-map-marker-alt"></i> Alamat :
                                    </label>
                                    <input type="text" name="alamat" value="{{ $siswa->alamat }}" />
                                </div>
<br>
                                <div class="toggle-code">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fas fa-save"></i> Simpan
                                    </button>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </section> -->
@endsection