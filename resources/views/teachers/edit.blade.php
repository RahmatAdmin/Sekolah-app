@extends('layouts.layout')
<link rel="stylesheet" href="{{ asset('css/style.css')}}">

@section('content')
<section style="width: 100%; display: flex; justify-content: center;">
    <main class="demo-page-content" style="width: 50%;">
        <section style="background-color: green;">
            <div class="href-target" id="installation"></div>
            <h2 style="color: white;">
            <i class="fas fa-user-edit" style="color: white;"></i>
                Update Data Guru
            </h2>
        </section>
        <form action="{{ route('guru.update', $guru->id) }}" method="POST">
            @csrf
            @method('PUT')
            <section>
                <div class="href-target" id="input-types"></div>
                <div class="nice-form-group">
                    <label for="name">
                        <i class="fas fa-user"></i> Nama Lengkap :
                    </label>
                    <input type="text" name="name" id="name" value="{{ $guru->name }}" class="form-control" />
                </div>

                <div class="nice-form-group">
                    <label for="nip">
                        <i class="fas fa-id-badge"></i> NIP :
                    </label>
                    <input type="text" name="nip" id="nip" value="{{ $guru->nip }}" class="form-control" />
                </div>

                <div class="nice-form-group">
                    <label for="email">
                        <i class="fas fa-envelope"></i> Email :
                    </label>
                    <input type="email" name="email" id="email" value="{{ $guru->email }}" class="form-control" />
                </div>

                <div class="nice-form-group">
                    <label for="wa">
                        <i class="fas fa-phone"></i> Whatsapp :
                    </label>
                    <input type="text" name="wa" id="wa" value="{{ $guru->wa }}" class="form-control" />
                </div>

                <div class="nice-form-group">
                    <label for="address">
                        <i class="fas fa-map-marker-alt"></i> Alamat :
                    </label>
                    <input type="text" name="address" id="address" value="{{ $guru->address }}" class="form-control" />
                </div>

                <div class="nice-form-group">
                    <label for="gender">
                        <i class="fas fa-venus-mars"></i> Gender :
                    </label>
                    <!-- <input type="text" name="gender" id="gender" value="{{ $guru->gender }}" class="form-control" /> -->
                    <select class="form-control" name="gender" id="gender" required>
                        <option value="Laki-laki" {{ $guru->gender == 'Laki-laki' ? 'selected' : '' }}>Laki-laki</option>
                        <option value="Perempuan" {{ $guru->gender == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                    </select>
                </div>

                <div class="nice-form-group">
                    <label for="kualifikasi">
                        <i class="fas fa-graduation-cap"></i> Kualifikasi :
                    </label>
                    <input type="text" name="kualifikasi" id="kualifikasi" value="{{ $guru->kualifikasi }}" class="form-control" />
                </div>

                <div class="nice-form-group text-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-save"></i> Simpan
                    </button>
                </div>
            </section>
        </form>
    </main>
</section>

<!-- Menyertakan FontAwesome -->
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>

<style>
    body {
        background-color: #f8f9fa;
        font-family: Arial, sans-serif;
    }
    .container {
        max-width: 800px;
        margin: 50px auto;
        padding: 40px;
        background-color: #fff;
        border-radius: 10px;
        box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.1);
    }
    h2 {
        color: #333;
        margin-bottom: 30px;
        text-align: center;
    }
    .nice-form-group {
        margin-bottom: 20px;
    }
    .nice-form-group label {
        font-weight: 600;
        margin-bottom: 5px;
        display: block;
    }
    .nice-form-group label i {
        margin-right: 8px;
        color: #4CAF50;
    }
    .form-control {
        width: 100%;
        padding: 10px;
        border-radius: 5px;
        border: 1px solid #ced4da;
        transition: border-color 0.3s;
    }
    .form-control:focus {
        border-color: #4CAF50;
        box-shadow: 0 0 5px rgba(76, 175, 80, 0.3);
    }
    .btn-primary {
        background-color: #4CAF50;
        border: none;
        padding: 10px 20px;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }
    .btn-primary i {
        margin-right: 5px;
    }
    .btn-primary:hover {
        background-color: #45a049;
    }
</style>
@endsection
