@extends('layouts.layout')
@section('content')

@if(session('success'))
<script>
    Swal.fire({
        title: 'success!',
        text: '{{ session('
        success ') }}',
        icon: 'success',
        customClass: {
            popup: 'swal2-popup'
        }
    });
</script>
@endif

<section style="padding: 20px;">
    <div class="table-responsive table-responsive-xl">
        <div class="card">
            <div style="background-color: #34495E"; class="card-header text-white">
                <h4 class="mb-0">Manage Data Guru</h4>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <button style="background-color: #34495E"; id="tambahDataBtn" class="btn text-white">Tambah Data</button>
                    <form action="{{ route('guru.search') }}" method="GET" class="form-inline">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Cari..." name="keyword">
                            <div class="input-group-append">
                                <button style="background-color: #34495E"; class="btn text-white" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr style="text-align: center;">
                                <th>No</th>
                                <th>Nama</th>
                                <th>Foto</th>
                                <th>NIP</th>
                                <th>Email</th>
                                <th>Whatsapp</th>
                                <th>Alamat</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Kualifikasi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: center;">
                            @foreach($dataguru as $index => $guru)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $guru->name }}</td>
                                <td>
                                    @if($guru->pas_foto)
                                    <img src="{{ asset('storage/' . $guru->pas_foto) }}" alt="Pas Foto" style="width: 100px; height: 100px; border-radius: 0%;">
                                    @else
                                    <p>Belum ada Pas Foto diunggah.</p>
                                    @endif
                                </td>
                                <td>{{ $guru->nip }}</td>
                                <td>{{ $guru->email }}</td>
                                <td>{{ $guru->wa }}</td>
                                <td>{{ $guru->address }}</td>
                                <td>{{ $guru->birthdate }}</td>
                                <td>{{ $guru->gender }}</td>
                                <td>{{ $guru->kualifikasi }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Aksi" style="display: flex; gap: 20px;">
                                        <button type="button" class="btn btn-sm btn-info">
                                            <a href="{{ route('guru.edit', $guru->id) }}"><i class="fas fa-edit"></i> Edit</a>
                                        </button>
                                        <button class="btn btn-sm btn-danger btn-delete" data-toggle="modal" data-target="#teachermodal{{ $guru->id }}" data-id="{{ $guru->id }}"><i class="fas fa-trash"></i> Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal Konfirmasi Hapus -->
                            <div class="modal fade" id="teachermodal{{ $guru->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger text-white">
                                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Hapus</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah Anda yakin ingin menghapus data ini?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <form id="deleteForm" method="POST" action="/guru/{{ $guru->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Yes</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Modal untuk form tambah data -->
<div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #34495E";>
                <h5 class="modal-title" id="exampleModalLabel" style="color: white;">Tambah Data Guru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Isi form tambah data di sini -->
                <form method="POST" action="{{ route('guru.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama Lengkap:</label>
                        <input type="text" class="form-control" id="name" placeholder="Masukkan nama lengkap" name="name">
                    </div>
                    <div class="form-group">
                        <label for="nip">NIP:</label>
                        <input type="text" class="form-control" id="nip" placeholder="Masukkan NIP" name="nip">
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender:</label>
                        <!-- <input type="text" class="form-control" id="gender" placeholder="Masukkan gender" name="gender"> -->
                        <select class="form-control" name="gender" id="gender" required>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan" selected>Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birthdate:</label>
                        <input type="date" class="form-control" id="birthdate" placeholder="Birthdate" name="birthdate">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" placeholder="Masukkan email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="wa">Whatsapp:</label>
                        <input type="text" class="form-control" id="wa" placeholder="Masukkan Whatsapp" name="wa">
                    </div>
                    <div class="form-group">
                        <label for="kualifikasi">Qualification:</label>
                        <input type="text" class="form-control" id="kualifikasi" placeholder="Masukkan kualifikasi" name="kualifikasi">
                    </div>
                    <!-- <div class="form-group">
        <label for="address">Alamat:</label>
        <input type="text" class="form-control" id="address" placeholder="Masukkan alamat" name="address">
    </div> -->
                    <div class="form-group">
                        <label for="pas_foto">Pas Foto:</label>
                        <input type="file" class="form-control-file" id="pas_foto" name="pas_foto" accept="image/*">
                    </div>
                    <button style="background-color: #34495E"; type="submit" class="btn text-white">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        $('#tambahDataBtn').click(function() {
            $('#tambahDataModal').modal('show');
        });

        $('#tambahDataModal').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
        });
    });
</script>

@endsection