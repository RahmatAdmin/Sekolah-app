<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>Document</title>
    <style>
           body {
            background-color: white;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        header {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            z-index: 1000;
        }
    </style>
</head>

<body>
<header class="p-3 mb-0 border-bottom bg-body">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-body">
            <a href="/" class="navbar-brand d-flex align-items-center mb-2 mb-lg-0 link-body-emphasis text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap">
                    <use xlink:href="#bootstrap" />
                </svg>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item me-3">
                        <img src="{{ asset('img/logo.jpeg')}}" alt="Logo" class="rounded-circle" style="width: 70px; height: 70px;">
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('page.head') }}" class="nav-link px-2 link-secondary">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/about" class="nav-link px-2 link-body-emphasis">Tentang</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('artikel.collect') }}" class="nav-link px-2 link-body-emphasis">Artikel</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('page.daftar') }}" class="nav-link px-2 link-body-emphasis">Pendaftaran</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('page.index') }}" class="nav-link px-2 link-body-emphasis">Website Kedua</a>
                    </li>
                </ul>
                <div class="d-flex align-items-center">
                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal" style="width: auto; margin-right: 20px;">Daftar Disini</button>
                    <div class="dropdown text-end">
                        <button type="button" class="btn btn-outline-success">
                            <a class="dropdown-item" href="/login">Login Admin</a>
                        </button>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>

    @yield('section')

    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
