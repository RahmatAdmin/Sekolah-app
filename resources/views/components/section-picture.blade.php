<!-- section documentation -->
<section class="container-fluid bg-light-subtle" style="background-color: green; min-height: 600px;">
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/active1.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/active2.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/active3.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/active4.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/active5.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4 d-flex justify-content-center">
                    <div class="bg-light d-flex justify-content-center align-items-center" style="width: 300px; height: 200px;">
                        <img src="{{ asset('img/bg.jpeg')}}" alt="" srcset="" style="width: 300px; height: 200px;">
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- end section documentation -->