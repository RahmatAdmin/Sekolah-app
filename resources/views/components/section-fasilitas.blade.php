<!-- section fasilitas -->
<div class="container" style="margin-top: 0px;">
        <ul class="list-group">
            <li class="list-group-item list-group-item-success" aria-current="true" style="font-weight: bold;">Fasilitas Yang Unggul</li>
            <li class="list-group-item">Ruang Asrama Full AC & Kipas Angin</li>
            <li class="list-group-item">Pembelajaran Di Dalam Dan Luar Kelas</li>
            <li class="list-group-item">1 Asatidz/ Guru Maksimal 10 Santri</li>
            <li class="list-group-item">Saung Tempat Belajar</li>
            <li class="list-group-item">Mushalla Putra/ Putri</li>
            <li class="list-group-item">Kantin</li>
            <li class="list-group-item">Lapangan Umum Olahraga</li>
            <li class="list-group-item">Dapur Umum</li>
            <li class="list-group-item">Pos Satpam</li>
        </ul>
    </div>
<!-- end section fasilitas -->