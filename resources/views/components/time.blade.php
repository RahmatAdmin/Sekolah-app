<div id="clock" class="text-center" style="font-size: 15px; display: flex; position: relative; left: 90%; width: 300px;"></div>
<script>
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var day = today.toLocaleDateString('id-ID', { weekday: 'long' }); // Mengambil nama hari dalam Bahasa Indonesia
        var date = today.toLocaleDateString('id-ID'); // Mengambil tanggal dalam format tanggal Indonesia
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('clock').innerHTML = h + ":" + m + ":" + s + "<br>" + day + ", " + date;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) { i = "0" + i }; // tambahkan angka 0 jika kurang dari 10
        return i;
    }
    startTime();
</script>
