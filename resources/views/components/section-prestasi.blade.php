  <!-- section prestasi -->
  <div class="container">
        <div>
            <h4>Prestasi Di Tingkat Kecamatan & Kabupaten</h4>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="list-group" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-bs-toggle="list" href="#list-home" role="tab" aria-controls="list-home">Juara 1 Hifdzil Qur'an 5 Juz Putri</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-bs-toggle="list" href="#list-profile" role="tab" aria-controls="list-profile">Juara 1 Hifdzil Qur'an 10 Juz Putra</a>
                    <a class="list-group-item list-group-item-action" id="list-messages-list" data-bs-toggle="list" href="#list-messages" role="tab" aria-controls="list-messages">Juara 1 Fahmil Qur'an</a>
                    <a class="list-group-item list-group-item-action" id="list-settings-list" data-bs-toggle="list" href="#list-settings" role="tab" aria-controls="list-settings">Juara 3 Harapan Hifdzil Qur'an 10 Juz Putra</a>
                </div>
            </div>
            <div class="col-8">
                <div class="tab-content" id="nav-tabContent" style="text-align: justify;">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">pada tahun 2023 santriwati perwakilan pondok pesantrent kami memperoleh juara 1 hifzdil qur'an di tingkat kecematan
                        <br>
                        <img src="{{ asset('img/logo.jpeg')}}" alt="" srcset="" style="width: 100px; height: 100px;">
                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">dan pada tahun 1 2023 santri prewakilan pondok pesantrent kami memperoleh juara 1 di tingkat kecamatan pada kategori 10 juz</div>
                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">kemudian pada tahun 2023 santri perwakilan pondok pesantren kami memperoleh juara 1 diperlombaan fahmil qur'an di tingkat kecematan </div>
                    <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">Dan pada tahun yang sama di tahun 2023 santri perwakilan pondok pesantren kami memperoleh juara harapan 3 diperlombaan hifdzil qur'an dikategori 10 juz putra ditingkat Kabupaten</div>
                </div>
            </div>
        </div>
    </div>
    <!-- end section prestasi -->