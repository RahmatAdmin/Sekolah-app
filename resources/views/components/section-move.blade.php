<!-- section move -->
<section class="container-fluid bg-light-subtle" style="background-color: green; height: 1000px; margin-top: 20px;">
        <div class="container h-100 d-flex flex-column justify-content-center">
            <div class="row align-items-center text-center text-md-left justify-content-between">
                <div class="col-md-4 mb-4 mb-md-0">
                    <img src="{{ asset('img/bismillah.png')}}" alt="" srcset="" class="img-fluid" style="height: 200px;">
                    <p class="text-justify text-black" style="text-align: justify;">
                        <span style="color: green; font-weight: ;"> Pesantren menyediakan lingkungan belajar yang positif, jauh dari gangguan dan pengaruh negatif.</span> Dengan bimbingan para ustadz dan ustadzah,
                        anak-anak akan lebih fokus dalam menuntut ilmu.
                        Mereka juga belajar hidup bersama dalam kebersamaan dan solidaritas, menghormati satu sama lain, bekerja sama,
                        dan mengembangkan sikap tolong-menolong, yang penting untuk kehidupan bermasyarakat.
                    </p>
                    <p class="text-justify text-black"  style="text-align: justify;">
                        Di <span style="color: green; font-weight: bold;"> pesantren Baitul Qur'an Bina Ukhuwah,</span> anak-anak akan mendapatkan bimbingan khusus dalam menghafal Al-Qur'an.
                        <span style="color: green; font-weight: bold;">Program tahfidz ini dirancang dengan metode yang efektif,
                            sehingga anak-anak dapat menghafal Al-Qur'an dengan baik dan benar.</span>Dengan hafalan Al-Qur'an, mereka akan memiliki pegangan hidup yang kuat dan menjadi generasi Qur'ani yang membanggakan.
                    </p>
                </div>

                <div class="col-md-6 text-center d-flex justify-content-center align-items-center" style="flex-direction: column;">
                    <img src="{{ asset('img/logo.jpeg')}}" alt="Image" class="img-fluid mb-3" style="width: 50%; height: 50%;">
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal" style="width: 250px; font-weight: bold;">Ayo Segera Daftar Di Sini</button>
                </div>
            </div>
        </div>
    </section>
<!-- end section move -->