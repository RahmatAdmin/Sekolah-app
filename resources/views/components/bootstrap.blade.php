<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<style>
       ::-webkit-scrollbar{
        display: none;
    }
    /* Customize here */
    header {
      background-color: white; /* Warna latar belakang navbar */
      color: black; /* Warna teks */
      height: 100px; /* Tinggi header */
      padding: 20px;
      position: fixed;
      width: 100%;
      top: 0;
      left: 0;
      z-index: 1000;
      transition: background-color 0.5s ease;
    }
    @media (max-width: 400px) {
      header {
        height: 50px;
        padding: 10px;
      }
    }
    .navbar-brand, .nav-link {
      color: black !important; /* Warna teks navbar */
    }
    .search-form {
      margin: 0 auto; /* Posisi tengah */
    }
</style>