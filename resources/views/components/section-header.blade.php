   <!-- header-section -->
   <div class="bgimg position-relative d-flex justify-content-center w-100">
        <div class="w-100 position-absolute top-0 start-0 h-100" style="background: rgba(0, 0, 0, 0.5);"></div>
        <img class="bg w-100" src="{{ asset('img/bg.jpeg')}}" alt="Background Image" style="height: 900px; object-fit: cover;">
        <div class="overlay-text position-absolute top-50 start-50 translate-middle text-center text-white animate__animated animate__backInLeft">
            <h2 id="text" class="animate__backInLeft display-4 display-md-3">Pondok Pesantren Baitul Qur'an</h2>
            <h1 class="animate__backInLeft display-4 display-md-3" style="color: #7FFFD4; font-weight: bolder;">Bina Ukhuwah</h1>
        </div>
    </div>
    <!-- end header section -->