    <!-- section visis dan misi -->
    <div class="container d-flex align-items-center justify-content-center" style="height: 500px;">
        <div class="row w-100">
            <div class="col-md-6 mb-3">
                <a class="btn btn-success w-100" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Klik untuk melihat Visi Pondok Baitul Qur'an
                </a>
                <div class="collapse mt-3" id="collapseExample">
                    <div class="card card-body">
                        Mencetak Generasi Penghafal Al Qur'an Yang Bertaqwa, Berilmu, Berprestasi dan Berakhlakul Karimah
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <a class="btn btn-success w-100" data-bs-toggle="collapse" href="#btn2" role="button" aria-expanded="false" aria-controls="btn2">
                    Klik untuk melihat Misi Pondok Baitul Qur'an
                </a>
                <div class="collapse mt-3" id="btn2">
                    <div class="card card-body">
                        <ul>
                            <li>Mendidik Peserta Didik Untuk Menjadi Penghafal Al-qur'an</li>
                            <li>Mengamalkan Al qur'an Dan Sunnah Rasulullah Saw Dalam Kehidupan sehari-hari</li>
                            <li>Membina Peserta Didik Agar Menjadi Manusia Yang Memiliki Kecakapan Hidup Dan Keterampilan Sosial, Teknologi Informasi Dan Komunikasi Secara Al Qur'an</li>
                            <li>Mempersiapkan Sekaligus Menjadikan Peserta Didik Lulusan Hafidz Dan Hafidzah Al qur'an Yang Berilmu Dan Berakhlakul Karimah</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end section visi misi -->