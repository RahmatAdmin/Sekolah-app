

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="{{ asset('Admin/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('Admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('Admin/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('Admin/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('Admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('Admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('Admin/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('Admin/dist/js/demo.js') }}"></script>
<script src="{{ asset('Admin/dist/js/pages/dashboard.js') }}"></script>
</body>
</html>
