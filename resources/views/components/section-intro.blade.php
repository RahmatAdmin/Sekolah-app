    <!-- section pengantar -->
    <section class="section-info bg-light-subtle py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0" style="display: flex; flex-direction: column; justify-content: center;">
                    <img src="{{ asset('img/mudhir.png') }}" alt="Gambar" class="img-fluid" style="width: 350px; height: auto;">
                    <div>
                        <span style="color: black; font-weight: bold;">Ustadz Fahrizal Nasution, S.Th.I Al Hafidz</span>
                        <p>Pimpinan Pondok Pesantrent Baitul Qur'an</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <h2 class="text-dark" style="text-decoration: underline;">Pengantar</h2>
                        <p class="text-dark text-justify" style="font-weight: bold; text-align: justify;">
                            <span style="color: green; font-weight: bold;">Selama Kurang Lebih 7 Tahun,Pondok Pesantren Baitul Qur’an Sudah Mewisudakan 18 Orang Santri/Santriwati Yang Hafidz/Hafidzah 30 Juz Alqur’an Dan Menamatkan Puluhan Santri Dengan Hafalan Yang Berfariasi.</span>
                            Dari Segi Prestasi Santri/Santriwati Telah Banyak Memenangkan Lomba Mtq Baik Tingkat Desa,Kecamatan,Dan Kabupaten Dari Berbagai Cabang Lomba Bahkan Pada Tahun 2022 Santri Ponpes Baitul Qur’an Meraih Juara 3 Lomba Hifdzil 5 Juz Antar Pondok Pesantren Seprovinsi Riau Sumatera Utara Yang Di Adakan Di Pondok Pesantren Al Majidiah Bagan Batu.
                            <br>
                            <span style="color: green; font-weight: bold;">
                                Namun,Selain Fokus Kepada Program Tahfidz Alqur’an,Ponpes Baitul Qur’an Juga Terfokus Pada Mengkaji Kitab-kitab Kuning. Saat ini pondok pesantren baitul qur’an sudah menerima peserta didik di jenjang :
                            </span>
                        <ul style="list-style: none; font-weight: bolder;">
                            <li>1.Pkpps Wustho ( Setingkat Smp ).</li>
                            <li>2.Pkpps Ulya ( Setingkat Sma ).</li>
                            <li>3.Mahasantri ( Setelah lulus Sma ).</li>
                        </ul>
                        </p>
                    </div>
                    <div class="d-flex mt-3">
                        <button type="button" class="btn btn-primary me-2" style="width: 100px;">Facebook</button>
                        <button type="button" class="btn btn-success me-2" style="width: 100px;">Whatsapp</button>
                        <button type="button" class="btn btn-danger" style="width: 100px;">Youtube</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section pengantar -->