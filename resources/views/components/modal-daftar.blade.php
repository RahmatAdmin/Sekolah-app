<!-- modal daftar -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-info-circle me-2"></i>Yuk Baca Dulu Syarat Dan Ketentuan Pendaftaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="scrollable-content">
                        <p style="font-weight: bold;"><i class="fas fa-check-circle me-2"></i>Syarat Pendaftaran</p>
                        <ul>
                            <li><i class="fas fa-caret-right me-2"></i>Mengisi Formulir Pendaftaran</li>
                            <li><i class="fas fa-caret-right me-2"></i>Foto Copy Akte Kelahiran 1 lembar</li>
                            <li><i class="fas fa-caret-right me-2"></i>Foto Copy KK 1 Lembar</li>
                            <li><i class="fas fa-caret-right me-2"></i>Foto Copy Surat Keterangan Lulus ( menyusul )</li>
                            <li><i class="fas fa-caret-right me-2"></i>Pas Foto 3x4 2 Lembar</li>
                            <li><i class="fas fa-caret-right me-2"></i>Memenuhi Administrasi Yang Telah Di Tentukan</li>
                        </ul>

                        <p style="font-weight: bold;"><i class="fas fa-star me-2"></i>Program Unggulan</p>
                        <ul>
                            <li><i class="fas fa-caret-right me-2"></i>Hafidz Al-qur'an 30 Juz Max 3 Thn</li>
                            <li><i class="fas fa-caret-right me-2"></i>Tasmi' 5 Juz Sekali Halaqah</li>
                            <li><i class="fas fa-caret-right me-2"></i>Tahsin Al-qur'an</li>
                            <li><i class="fas fa-caret-right me-2"></i>Kelas Takhosus</li>
                            <li><i class="fas fa-caret-right me-2"></i>Muhadharah</li>
                            <li><i class="fas fa-caret-right me-2"></i>Kitab Kuning ( klasik ) </li>
                        </ul>

                        <p style="font-weight: bold;"><i class="fas fa-phone-alt me-2"></i>Contact Us</p>
                        <ul>
                            <li><i class="fas fa-caret-right me-2"></i>Ustadz Fahrizal, S.Th.i : 0823 6539 9161</li>
                            <li><i class="fas fa-caret-right me-2"></i>Ustadz Rismanda, S.Pd.i : 0852 6544 2411</li>
                            <li><i class="fas fa-caret-right me-2"></i>Bapak Sahrum : 0821 7079 5257</li>
                            <li><i class="fas fa-caret-right me-2"></i>Ustadzah Nurul, S.Pd : 0822 7621 4989</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"><a href="{{ route('page.daftar') }}" style="color: white; text-decoration: none;">Daftar</a></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal daftar -->