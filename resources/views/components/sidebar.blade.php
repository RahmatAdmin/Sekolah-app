<style>
    a {
        color: white;
    }

    p {
        color: white;
    }
</style>
<!-- SIDEBAR NAVIGATION  -->
<aside class="main-sidebar" style="background-color: #34495e; color: white;">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('Admin/dist/img/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Baitul Qur'an</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">

            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <!-- LAYOUT -->
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy" style="color: white;"></i>
                        <p>
                            Layout Options
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="" class="nav-link">
                                <p>Custom Area</p>
                            </a>
                        </li>
                        <li class="nav-item" id="errorButton">
                            <a href="{{ route('custom.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Landing Page</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- END LAYOUT -->


                <li class="nav-header">MANAGE DATA</li>
                <li class="nav-item">
                    <a href="{{ route('dashboard')}}" class="nav-link  ">
                        <i class="nav-icon fas fa-tachometer-alt" style="color: white;"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('dashboard.index')}}" class="nav-link ">
                        <i class="nav-icon fas fa-user-graduate" style="color: white;"></i>
                        <p>
                            Siswa
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('guru.index')}}" class="nav-link ">
                        <i class="nav-icon fas fa-chalkboard-teacher" style="color: white;"></i>
                        <p>
                            Guru
                        </p>
                    </a>
                </li>


                <li class="nav-item">
    <a href="{{ route('dashboard.artikel')}}" class="nav-link ">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="nav-icon" fill="white" style="color: white;">
            <path d="M0 0h24v24H0z" fill="none" />
            <path d="M6 2c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V8l-6-6H6zm6 7V3.5L18.5 9H12zm-2 7H8v-2h2v2zm6 0h-4v-2h4v2zm0-4H8v-2h8v2z"/>
        </svg>
        <p>
            Artikel
        </p>
    </a>
</li>


                <li class="nav-item">
    <a href="{{ route('pendaftaran.index')}}" class="nav-link ">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="nav-icon" fill="white" style="color: white;">
            <path d="M0 0h24v24H0z" fill="none"/>
            <path d="M12 12c2.67 0 8 1.34 8 4v2H4v-2c0-2.66 5.33-4 8-4zm0-2c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm0-10C6.48 0 2 4.48 2 10s4.48 10 10 10 10-4.48 10-10S17.52 0 12 0z"/>
        </svg>
        <p>
           Pendaftaran
        </p>
    </a>
</li>

            </ul>
        </nav>
    </div>
</aside>

<!-- END SIDEBAR NAVIGATION  -->