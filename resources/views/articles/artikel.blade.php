@extends('layouts.layout')
@section('content')

@if(session('success'))
        <script>
            Swal.fire({
                title: 'success!',
                text: '{{ session('success') }}',
                icon: 'success',
                customClass: {
                    popup: 'swal2-popup'
                }
            });
        </script>
    @endif


<section style="padding: 20px;">
    <div class="table-responsive">
        <div class="card">
            <div style="background-color: #34495e;" class="card-header text-white">
                <h4 class="mb-0">Manage Data Artikel</h4>
            </div>
            <div class="card-body">


                <div class="d-flex justify-content-between mb-3">
                    <button style="background-color: #34495e;" id="tambahDataBtn" class="btn">
                        <a href="{{ route('artikel.create')}}">Tambah Data</a>
                    </button>
                    <form action="{{ route('artikel.search') }}" method="GET" class="form-inline">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Cari..." name="keyword">
                            <div class="input-group-append">
                                <button style="background-color: #34495e;" class="btn text-white" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>


                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead style="background-color: #34495e;" class="text-white">
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Gambar</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articleframe as $index => $artikel)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $artikel->tittle }}</td>
                                <td>{{ $artikel->desc }}</td>
                                <td>
                                    <img src="{{ asset('storage/' . $artikel->img) }}" alt="img" style="max-width: 150px; max-height: 100px;">
                                </td>
                                <td>{{ $artikel->date }}</td>
                                <td>


                                    <div class="btn-group" role="group" aria-label="Aksi" style="display: flex; gap: 50px;">
                                        <button type="button" class="btn btn-sm btn-info" style="width: 5px;">
                                        <a href="{{ route('artikel.edit', $artikel->id) }}">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </button>
                                        <button class="btn btn-sm btn-danger btn-delete" data-toggle="modal" data-target="#confirmationModaldelete" data-id="{{ $artikel->id }}" style="width: 5px;">
                                            <i class="fas fa-trash"></i>Delete
                                        </button>
                                    </div>


                                    <!-- modal delete -->
                                    <div class="modal fade" id="confirmationModaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header bg-danger text-white">
                                                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Hapus</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Apakah Anda yakin ingin menghapus data ini?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <form id="deleteForm" method="POST" action="/artikel/{{ $artikel->id }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Yes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end modal -->


                </div>
                </td>
                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</section>

@endsection