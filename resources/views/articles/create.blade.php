@extends('layouts.layout')
@section('content')
<section>
    <style>
        body {
            background-color: #f8f9fa;
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 800px;
            margin: 50px auto;
            padding: 40px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #333;
            margin-bottom: 30px;
            text-align: center;
        }
        .form-group {
            margin-bottom: 20px;
        }
        .form-group label {
            font-weight: 600;
            margin-bottom: 5px;
            display: block;
        }
        .form-control {
            border-radius: 5px;
            padding: 10px;
            font-size: 16px;
            border: 1px solid #ced4da;
            transition: border-color 0.3s;
        }
        .form-control:focus {
            border-color: #4CAF50;
            box-shadow: 0 0 5px rgba(76, 175, 80, 0.3);
        }
        .form-control-file {
            border: none;
            padding: 10px 0;
        }
        .file-upload-card {
            position: relative;
            overflow: hidden;
            margin-top: 10px;
            border: 2px dashed #ccc;
            border-radius: 5px;
            background-color: #f8f9fa;
            padding: 20px;
            text-align: center;
            cursor: pointer;
            transition: border-color 0.3s;
        }
        .file-upload-card:hover {
            border-color: #4CAF50;
        }
        .file-upload-card input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
        }
        .file-upload-icon {
            font-size: 24px;
            margin-bottom: 10px;
            color: #4CAF50;
        }
        .file-upload-text {
            color: #888;
        }
        .btn-primary {
            background-color: #4CAF50;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        .btn-primary:hover {
            background-color: #45a049;
        }
    </style>
    <div class="container">
        <h2>Create Article</h2>
        <form action="{{ route('artikel.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Judul:</label>
                <input type="text" class="form-control" id="tittle" name="tittle" required>
            </div>
            <div class="form-group">
                <label for="description">Deskripsi:</label>
                <textarea class="form-control" id="desc" name="desc" rows="5" required></textarea>
            </div>
            <div class="form-group">
                <label for="image">Gambar:</label>
                <div class="file-upload-card">
                    <span class="file-upload-icon">&#128247;</span>
                    <span class="file-upload-text">Drag & drop or click to upload image</span>
                    <input type="file" class="form-control-file" id="img" name="img" required>
                </div>
            </div>
            <div class="form-group">
                <label for="content">Konten Artikel</label>
                <textarea class="form-control" id="content" name="content" rows="10">Welcome to TinyMCE!</textarea>
            </div>
            <div class="form-group">
                <label for="date">Tanggal:</label>
                <input type="date" class="form-control" id="date" name="date" required>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</section>
@endsection
