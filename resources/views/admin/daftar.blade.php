@extends('layouts.layout')
@section('content')


<section style="padding: 20px;">
    <div class="table-responsive table-responsive-xl" style="width: 100%; height: 800px;">
        <div class="row">
            <div style="width: 100%;">
                <div class="card">
                    <div style="background-color: #34495E" class="card-header text-white">
                        <h4 class="mb-0">Manage Data Calon Siswa</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Cari..." name="keyword">
                                    <div class="input-group-append">
                                        <button style="background-color: #34495E"; class="btn text-white" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="table-responsive table-responsive-xl">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>TTL</th>
                                        <th>Alamat</th>
                                        <th>Skl</th>
                                        <th>Akte</th>
                                        <th>KK</th>
                                        <th>Pas Foto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($casis as $index => $siswa)
                                    <tr style="height: 80px;">
                                        <td>{{ $index +1 }}</td>
                                        <td>{{ $siswa->nama_lengkap }}</td>
                                        <td>{{ $siswa->tanggal_lahir }}</td>
                                        <td>{{ $siswa->alamat}}</td>
                                        <td>
                                        @if($siswa->skl)
                                            <img src="{{ asset('storage/' . $siswa->skl) }}" alt="SKL" style="width: 150px; height: 150px; border-radius: 0%;">
                                            @else
                                            <p>Belum ada SKL diunggah.</p>
                                            @endif
                                        </td>
                                        <td>
                                        @if($siswa->akte)
                                            <img src="{{ asset('storage/' . $siswa->akte) }}" alt="Akte" style="width: 150px; height: 150px; border-radius: 0%;">
                                            @else
                                            <p>Belum ada Akte diunggah.</p>
                                            @endif
                                        </td>
                                        <td>
                                        @if($siswa->kk)
                                            <img src="{{ asset('storage/' . $siswa->kk) }}" alt="KK" style="width: 150px; height: 150px; border-radius: 0%;">
                                            @else
                                            <p>Belum ada KK diunggah.</p>
                                            @endif
                                        </td>
                                        <td>
                                        @if($siswa->pas_foto)
                                            <img src="{{ asset('storage/' . $siswa->pas_foto) }}" alt="Pas Foto" style="width: 150px; height: 150px; border-radius: 0%;">
                                            @else
                                            <p>Belum ada Pas Foto diunggah.</p>
                                            @endif
                                        </td>
                                                <td>
        <form action="{{ route('siswa.accept', $siswa->id) }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-success">verifikasi pendaftaran</button>
        </form>
    </td>
                                            </div>
                                      
                                    </tr>
@endforeach
@endsection
