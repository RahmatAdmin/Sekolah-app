@extends('layouts.layout')
<style>
    body {
        background-color: #F5FFFA;
    }
</style>

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-3 col-6">  

            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $totalSiswa }}</h3>
                    <h5>Siswa</h5>
                </div>
                <div class="icon">
                    <svg class="icon-siswa" width="24" height="24">
                        <use xlink:href="nama_file_svg_icon_siswa.svg#nama_id_ikon_siswa"></use>
                    </svg>
                    <i class="fas fa-user-graduate"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>


        <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{ $totalGuru }}<sup style="font-size: 20px"></sup></h3>
                    <h5>Guru</h5>
                </div>
                <div class="icon">
                    <i class="fas fa-chalkboard-teacher"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>



        <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>33</h3>
                    <h5>Gedung</h5>
                </div>
                <div class="icon">
                    <i class="fas fa-building"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>


        <div class="col-lg-3 col-6">
            <div class="small-box bg-dark">
                <div class="inner">
                    <h3>{{ $totalArtikel }}</h3>
                    <h5>Artikel</h5>
                </div>
                <div class="icon">
                    <i class="fas fa-newspaper"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        @endsection('content')