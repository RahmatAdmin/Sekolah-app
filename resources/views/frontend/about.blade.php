@extends('layouts.landing')
@section('section')
<div class="container" style="margin-top: 150px;">
    <div class="row">
        <div class="col">
            <div class="img-wrapper" style="display: flex; justify-content: center;">
                <img src="{{ asset('img/logo.jpeg')}}" class="img-fluid" alt="Responsive image" style="width: 400px; height: 400px;">
            </div>
        </div>
    </div>
</div>

<div class="container" style="text-align: justify; width: 600px; font-weight: 500;>
    <p">
    Pondok  Pesantren Baitul Qur’an Terletak Di Desa Sungai Meranti Kec.Pinggir Kab.Bengkalis,Satu Kecamatan Yang Langsung Berbatasan Dengan Kab.Siak.
     Pada Tahun 2010 Pondok Pesantren Ini Bernama Lembaga Tahsin Dan Tahfidz Swadaya Masyarakat Desa Sungai Meranti,
    Yang Mana Pencetus Pertama Dari Lembaga Ini Adalah Ustadz.H Umar Mahmud.Lc. 
    Pada Awalnya Lembaga Tahsin dan Tahfidz Ini Bergerak Di Bidang Tulis,
    Baca,Dan Menghafal Alqur’an Yang Di Peruntukan Bagi Anak-Anak Desa Sungai Meranti Dan Sekitarnya.
    </p>
    <p>
    Kemudian 6 Tahun Berselang, Pada Tahun 2016 Lembaga Tahsin Dan Tahfidz Mendapat Tanah Hibah Seluas 2 HA Berupa Kebun Sawit Yang Di Hibahkan Oleh ( Alm ) Bapak Alimudin dan ( Almh ) Ibuk Maemunah warga Desa Sungai Meranti  
    Yang Menjadi Tempat Berdirinya Pondok Pesantren Baitul Qur’an Sekarang.Satu Tahun Berselang Tepatnya Pada Tanggal 24 Oktober 2017 Lembaga Tahsin Dan Tahfidz Alqur’an 
    Secara Resmi Membuka Pendaftaran Untuk Santri Mukim Angkatan Pertama Yang Berjumlah 5 Orang Dengan Full Beasiswa Penuh.
    </p>
    <p>
    Selama Kurang Lebih 7 Tahun,Pondok Pesantren Baitul Qur’an Sudah Mewisudakan 18 Orang Santri/Santriwati Yang Hafidz/Hafidzah
     30 Juz Alqur’an Dan Menamatkan Puluhan Santri Dengan Hafalan Yang Berfariasi. 
    </p>
    <p>
    Saat Ini Pondok Pesantren Baitul Qur’an Di Asuh Oleh Ustadz.Fahrizal Nasuiton,S.Th.I Al-Hafidz. Beliau Merupakan Alumni Dari
     Salah Satu Pondok Pesantren Ternama Yang Ada Di Sumatera Utara,Beliau Juga Telah Menempuh Pendidikan Strata 1 Di
     Institut Agama Islam Negeri Sumatera Utara Yang Mana Sekarang Berubah Menjadi  Universitas Islam Negeri Sumatera Utara.
    </p>
</div>

<x-section-footer></x-section-footer>
@endsection
