    @extends('layouts.landing')
    @section('section')

    <x-section-header></x-section-header>

    <x-section-vm></x-section-vm>

    <section class="section-info bg-light-subtle py-5">
    <div class="container">
        <div class="row">
              <div class="col-md-6 mb-4 mb-md-0" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                @foreach($data as $dataset)
                    @if($dataset->{'dropzone-file'})
                        <img src="{{ asset('storage/' . $dataset->{'dropzone-file'}) }}" alt="SKL" class="img-fluid" style="width: 350px; height: auto;">
                    @else
                        <p>Belum ada SKL diunggah.</p>
                    @endif
                    <div>
                        <span style="color: black; font-weight: bold;">Ustadz Fahrizal Nasution, S.Th.I Al Hafidz</span>
                        <p>Pimpinan Pondok Pesantren Baitul Qur'an</p>
                    </div>
            </div>

            <div class="col-md-6">
                <div>
                    <h2 class="text-dark" style="text-decoration: underline;">Pengantar</h2>
                    <p class="text-dark text-justify" style="font-weight: 100px; text-align: justify;">
                        <span style="color: green; font-weight: ;">Selama Kurang Lebih 7 Tahun, Pondok Pesantren Baitul Qur’an Sudah Mewisudakan 18 Orang Santri/Santriwati Yang Hafidz/Hafidzah 30 Juz Alqur’an Dan Menamatkan Puluhan Santri Dengan Hafalan Yang Berfariasi.</span>
                        Dari Segi Prestasi Santri/Santriwati Telah Banyak Memenangkan Lomba Mtq Baik Tingkat Desa, Kecamatan, Dan Kabupaten Dari Berbagai Cabang Lomba Bahkan Pada Tahun 2022 Santri Ponpes Baitul Qur’an Meraih Juara 3 Lomba Hifdzil 5 Juz Antar Pondok Pesantren Seprovinsi Riau Sumatera Utara Yang Di Adakan Di Pondok Pesantren Al Majidiah Bagan Batu.
                        <br>
                        <span style="color: green; font-weight: ;">
                            Namun, Selain Fokus Kepada Program Tahfidz Alqur’an, Ponpes Baitul Qur’an Juga Terfokus Pada Mengkaji Kitab-kitab Kuning. Saat ini pondok pesantren baitul qur’an sudah menerima peserta didik di jenjang:
                        </span>
                    <ul style="list-style: none; font-weight: bolder;">
                        <li>1. Pkpps Wustho ( Setingkat SMP ).</li>
                        <li>2. Pkpps Ulya ( Setingkat SMA ).</li>
                        <li>3. Mahasantri ( Setelah lulus SMA ).</li>
                    </ul>
                    </p>
                </div>
                <div class="d-flex mt-3">
                    <button type="button" class="btn btn-primary me-2" style="width: 100px;">Facebook</button>
                    <button type="button" class="btn btn-success me-2" style="width: 100px;">Whatsapp</button>
                    <button type="button" class="btn btn-danger" style="width: 100px;">Youtube</button>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- end section pengantar -->

     <!-- section count -->
    <div class="section-box bg-light-subtle py-5">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-3 mb-4 mb-md-0">
                    <div>
                        <h3 class="text-dark">20</h3>
                        <h5 class="text-success">Alumni</h5>
                    </div>
                </div>
                <div class="col-md-3 mb-4 mb-md-0">
                    <div>
                        <h3 class="text-dark">20</h3>
                        <h5 class="text-success">Gedung</h5>
                    </div>
                </div>
                <div class="col-md-3 mb-4 mb-md-0">

                    <div>
                        <h3 class="text-dark">{{ $dataset->{'quantity-input'} }}</h3>
                        <h5 class="text-success">Santri</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <h3 class="text-dark">11</h3>
                        <h5 class="text-success">Pengajar</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end section count -->
    @endforeach
    <x-section-picture></x-section-picture>
    <x-section-prestasi></x-section-prestasi>
    <x-section-move></x-section-move>
    <x-modal-daftar></x-modal-daftar>
    <x-section-fasilitas></x-section-fasilitas>
    <x-section-footer></x-section-footer>
    @endsection