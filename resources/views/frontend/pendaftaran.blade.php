@extends('layouts.landing')

<div class="container d-flex">
<div style="margin-top: 23vh; width: 100%;">
    
    <h2 style="margin-bottom: 20px;">{{ $title ?? "Data Diri" }}</h2>
    <form action="{{ $btn['url'] }}" method="POST" style="display: flex; justify-content: center; gap: 12px; flex-direction: column" enctype="multipart/form-data">
        @csrf
        {{-- Tahap 1 --}}
        @include('frontend.Elements.pendaftaran.pribadi', [
            'display' => $display['pribadi'],
            'value' => $value['pribadi']
        ])
        {{-- Tahap 2 --}}
        @include('frontend.Elements.pendaftaran.orangtua', [
            'ayahOrIbu' => 'Ayah',
            'display' => $display['ayah'],
            'prefix' => 'ayah',
            'value' => $value['ayah']
        ])
        {{-- Tahap 3 --}}
        @include('frontend.Elements.pendaftaran.orangtua', [
            'ayahOrIbu' => 'Ibu',
            'display' => $display['ibu'],
            'prefix' => 'ibu',
            'value' => $value['ibu']
        ])
        {{-- Tahap 4 --}}
        @include('frontend.Elements.pendaftaran.berkas', [
            'display' => $display['berkas'],
            'value' => $value['berkas']
        ])
        <div>
            @if ($step != 1)
                <button type="button" class="btn btn-secondary" onclick="window.history.back()">Kembali</button>
            @endif
            <button type="submit" class="btn btn-primary">{{ $btn['title'] }}</button>
        </div>
    </form>
</div>
</div>
