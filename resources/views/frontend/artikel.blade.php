@extends('layouts.landing')
<style>
    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

    .card {
        animation: fadeIn 0.5s ease-out;
    }
</style>

@section('section')

<!-- section card artikle -->
<section style="width: 100%; height: auto; margin-top: 20px; padding: 10%;">
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 g-4">
        @foreach($allartikel as $artikel)
        <div class="col">
            <div class="card h-100" style="border: none;">
                <img src="{{ asset('storage/' . $artikel->img) }}" class="card-img-top" alt="Article Image" style="border-top-left-radius: 0; border-top-right-radius: 0; height: 200px; object-fit: cover;">
                <div class="card-body">
                    <h5 class="card-title" style="color: green;">{{ $artikel->tittle }}</h5>
                    <p class="card-text" style="color: black;">{{ $artikel->desc }}</p>
                </div>
                <div class="card-footer" style="background-color: white; border: none;">
                    <small style="background-color: white;" class="text-muted">Published: {{ $artikel->date }}</small>
                    <a href="{{ route('artikel.detail', $artikel->id) }}" class="btn btn-success btn-sm float-end">Read More</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!-- end section card artikle -->

<!-- modal daftar -->
<x-modal-daftar></x-modal-daftar>
<!-- modal daftar -->

<x-section-footer></x-section-footer>
@endsection
