@extends('layouts.landing')

<div class="container d-flex">
<div style="margin-top: 23vh; width: 100%;">
    <h1 style="margin-bottom: 20px;">{{ $title ?? "Step 1 - Personal Data" }}</h1>
    <form action="{{ $btn['url'] }}" method="POST" style="display: flex; justify-content: center; gap: 12px; flex-direction: column" enctype="multipart/form-data">
        @csrf
        @include('frontend.Elements.pendaftaran.berkas', [
            'display' => 'display: block;',
            'value' => $value
        ])
        <div>
            <button type="submit" class="btn btn-primary">{{ $btn['title'] }}</button>
        </div>
    </form>
</div>
</div>
