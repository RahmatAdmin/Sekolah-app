<div class="row g-3">
        <div class="col-12 mb-4" style="{{ $display }}">
        <h2 style="margin-bottom: 20px;">{{ $title ?? "Data $ayahOrIbu" }}</h2>
            <label for="nama-lengkap-{{ $prefix }}" class="form-label">Nama Lengkap {{ $ayahOrIbu }}</label>
            <input type="text" class="form-control" id="nama-lengkap-{{ $prefix }}" name="nama-lengkap-{{ $prefix }}" value="{{ old('nama-lengkap-' . $prefix) }}">
            @error('nama-lengkap-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('nama-lengkap-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="tanggal-lahir-{{ $prefix }}" class="form-label">Tanggal Lahir</label>
            <input type="date" class="form-control border-0 shadow-sm focus:ring-2 focus:ring-indigo-500" id="tgl-lahir-{{ $prefix }}" name="tgl-lahir-{{ $prefix }}" value="{{ old('tgl-lahir-' . $prefix) }}">
            @error('tgl-lahir-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('tgl-lahir-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="alamat-{{ $prefix }}" class="form-label">Alamat</label>
            <input type="text" class="form-control" id="alamat-{{ $prefix }}" name="alamat-{{ $prefix }}" value="{{ old('alamat-' . $prefix) }}">
            @error('alamat-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('alamat-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="agama-{{ $prefix }}" class="form-label">Agama</label>
            <input type="text" class="form-control" id="agama-{{ $prefix }}" name="agama-{{ $prefix }}" value="{{ old('agama-' . $prefix) }}" >
            @error('agama-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('agama-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="pendidikan-terakhir-{{ $prefix }}" class="form-label">Pendidikan Terakhir</label>
            {{-- <input type="text" class="form-control" id="pendidikan-{{ $prefix }}" name="pendidikan-{{ $prefix }}" value="{{ old('pendidikan') }}"> --}}
            <select class="form-control" name="pendidikan-{{ $prefix }}" id="pendidikan-{{ $prefix }}" value="{{ old('pendidikan-' . $prefix) }}">
                <option value="sd">SD</option>
                <option value="smp">SMP</option>
                <option value="sma">SMA</option>
                <option value="d1">D1</option>
                <option value="d2">D2</option>
                <option value="d3">D3</option>
                <option value="s1">S1</option>
                <option value="s2">S2</option>
                <option value="s3">S3</option>
            </select>
            @error('pendidikan-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('pendidikan-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="pekerjaan-{{ $prefix }}" class="form-label">Pekerjaan</label>
            <input type="text" class="form-control" id="pekerjaan-{{ $prefix }}" name="pekerjaan-{{ $prefix }}" value="{{ old('pekerjaan-' . $prefix) }}">
            @error('pekerjaan-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('pekerjaan-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="penghasilan-{{ $prefix }}" class="form-label">Rata-rata penghasilan</label>
            {{-- <input type="number" class="form-control" id="penghasilan-{{ $prefix }}" name="penghasilan-{{ $prefix }}" value="{{ old('penghasilan' . $prefix) }}"> --}}
            <select class="form-control" name="penghasilan-{{ $prefix }}" id="penghasilan-{{ $prefix }}" value="{{ old('penghasilan-' . $prefix) }}">
                <option value="< 500000">Lebih kecil dari 500.000</option>
                <option value=">= 500000">Lebih besar dari atau sama dengan 500.000</option>
                <option value="1000000 - 5000000">1.000.000 - 5.000.000</option>
                <option value="> 5000000">Lebih besar dari 5.000.000</option>
            </select>
            @error('penghasilan-' . $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('penghasilan-' . $prefix) }}
            </div>
            @enderror
        </div>

        <div class="col-12 mb-4" style="{{ $display }}">
            <label for="no-telpon-{{ $prefix }}" class="form-label">No telpon</label>
            <input type="number" class="form-control" id="no-telpon-{{ $prefix }}" name="no-telpon-{{ $prefix }}" value="{{ old('no-telpon-' . $prefix) }}">
            @error('no-telpon-'. $prefix)
            <div class="alert alert-danger">
                {{ $errors -> first('no-telpon-' . $prefix) }}
            </div>
            @enderror
        </div>
    </div>
