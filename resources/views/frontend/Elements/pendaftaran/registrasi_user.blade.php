<div class="row g-3">
    <label style="{{ $display }}" for="form-label">Registrasi User untuk mendapat notifikasi</label>
    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="email" class="form-label text-lg font-medium text-gray-700">Email</label>
        <input type="text" class="form-control border-0 shadow-sm focus:ring-2 focus:ring-indigo-500"
            name="email" id="email" placeholder="Masukan Email" value="{{ old('email') }}">
        @error('email')
            <div class="alert alert-danger">
                {{ $errors -> first('email')}}
            </div>
        @enderror
    </div>
    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="password" class="form-label text-lg font-medium text-gray-700">Password</label>
        <input type="password" class="form-control border-0 shadow-sm focus:ring-2 focus:ring-indigo-500"
            name="password" id="password" placeholder="Masukan password" value="{{ old('password') }}">
        @error('password')
            <div class="alert alert-danger">
                {{ $errors -> first('password')}}
            </div>
        @enderror
    </div>
</div>
