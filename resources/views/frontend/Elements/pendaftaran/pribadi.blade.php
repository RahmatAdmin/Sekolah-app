<div class="row g-3">
    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="nama-lengkap" class="form-label fw-medium text-secondary">Nama Lengkap</label>
        <input type="text" class="form-control shadow-sm" name="nama-lengkap" id="nama-lengkap" placeholder="Masukkan Nama Lengkap" value="{{ old('nama-lengkap') }}">
        @error('nama-lengkap')
            <div class="alert alert-danger mt-2">{{ $errors->first('nama-lengkap') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="nama-laqob" class="form-label fw-medium text-secondary">Nama Laqob</label>
        <input type="text" class="form-control border border-primary shadow-sm" name="nama-laqob" id="nama-laqob" placeholder="Masukkan Nama Laqob" value="{{ old('nama-laqob') }}">
        @error('nama-laqob')
            <div class="alert alert-danger mt-2">{{ $errors->first('nama-laqob') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="jenis_kelamin" class="form-label fw-medium text-secondary">Jenis Kelamin</label>
        <select class="form-select" name="gender">
            <option value="Laki-laki" {{ old('gender') == 'Laki-laki' ? 'selected' : '' }}>Laki-laki</option>
            <option value="Perempuan" {{ old('gender') == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
        </select>
        @error('gender')
            <div class="alert alert-danger mt-2">{{ $errors->first('gender') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="asal-sekolah" class="form-label fw-medium text-secondary">Nama Asal Sekolah</label>
        <input type="text" class="form-control shadow-sm" name="asal-sekolah" id="asal-sekolah" placeholder="Masukkan Nama Asal Sekolah" value="{{ old('asal-sekolah') }}">
        @error('asal-sekolah')
            <div class="alert alert-danger mt-2">{{ $errors->first('asal-sekolah') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="alamat" class="form-label fw-medium text-secondary">Alamat</label>
        <input type="text" class="form-control shadow-sm" name="alamat" id="alamat" placeholder="Masukkan Alamat" value="{{ old('alamat') }}">
        @error('alamat')
            <div class="alert alert-danger mt-2">{{ $errors->first('alamat') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="tgl-lahir" class="form-label text-lg font-medium text-gray-700">Tanggal Lahir</label>
        <input type="date" class="form-control border-0 shadow-sm focus:ring-2 focus:ring-indigo-500"
            name="tgl-lahir" id="tgl-lahir" value="{{ old('tglLahir') }}">
        @error('tgl-lahir')
            <div class="alert alert-danger mt-2">{{ $errors->first('tgl-lahir') }}</div>
        @enderror
    </div>

    <div class="col-12 mb-4" style="{{ $display }}">
        <label for="no-tlp" class="form-label text-lg font-medium text-gray-700">No Telpon</label>
        <input type="number" class="form-control border-0 shadow-sm focus:ring-2 focus:ring-indigo-500" name="no-tlp"
            id="no-tlp" placeholder="Masukkan No Telpon" value="{{ old('noTlp') }}">
        @error('no-tlp')
            <div class="alert alert-danger mt-2">{{ $errors->first('no-tlp') }}</div>
        @enderror
    </div>
</div>
