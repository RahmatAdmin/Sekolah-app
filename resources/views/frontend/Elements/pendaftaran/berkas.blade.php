
<h2 style="margin-bottom: 20px;">{{ $title ?? "Berkas" }}</h2>
<div class="col-12 mb-4" style="{{ $display }}">
    <label for="berkas1" class="form-label">Upload Akte Kelahiran</label>
    {{-- <input type="file" class="form-control-file" id="akte_lahir" name="akte_lahir" value="{{ $value['akte_lahir'] }}"> --}}
    <input type="file" class="form-control" id="akte_lahir" name="akte_lahir">
    @if ($errors->has('akte_lahir'))
        <div class="alert alert-danger">
            {{ $errors->first('akte_lahir') }}
        </div>
    @endif
</div>


<div class="col-12 mb-4" style="{{ $display }}">
    <label for="berkas1" class="form-label">Upload Kartu Keluarga</label>
    {{-- <input type="file" class="form-control-file" id="kartu_keluarga" name="kartu_keluarga" value="{{ $value['kartu_keluarga'] }}"> --}}
    <input type="file" class="form-control" id="kartu_keluarga" name="kartu_keluarga">
    @if ($errors->has('kartu_keluarga'))
        <div class="alert alert-danger">
            {{ $errors->first('kartu_keluarga') }}
        </div>
    @endif
</div>


<div class="col-12 mb-4" style="{{ $display }}">
    <label for="berkas1" class="form-label">Upload Pas Foto</label>
    {{-- <input type="file" class="form-control-file" id="pas_foto" name="pas_foto" value="{{ $value['pas_foto'] }}"> --}}
    <input type="file" class="form-control" id="pas_foto" name="pas_foto">
    @if ($errors->has('pas_foto'))
        <div class="alert alert-danger">
            {{ $errors->first('pas_foto') }}
        </div>
    @endif
</div>

<div class="col-12 mb-4" style="{{ $display }}">
    <label for="berkas1" class="form-label">Upload Ijazah / SKL</label>
    {{-- <input type="file"  class="form-control-file" id="ijazah_skl" name="ijazah_skl" value="{{ $value['ijazah_skl'] }}"> --}}
    <input type="file"  class="form-control" id="ijazah_skl" name="ijazah_skl">
    @if ($errors->has('ijazah_skl'))
    <div class="alert alert-danger">
        {{ $errors->first('ijazah_skl') }}
    </div>
    @endif
</div>

<div class="col-12 mb-4" style="{{ $display }}">
    <label for="nis" class="form-label">NISN</label>
    <input type="number" class="form-control" id="nisn" name="nisn" value="{{ old('nisn') }}">
</div>
