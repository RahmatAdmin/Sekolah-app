@extends('layouts.landing')
@section('section')

<section class="mt-5">
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-0 shadow-lg">
                    <div class="card-header bg-success text-white text-center py-4">
                        <h2>Judul: {{ $article->tittle }}</h2>
                    </div>
                    <div class="card-body p-4">
                        <p class="card-subtitle mb-3 text-muted">Date: {{ $article->date }}</p>
                        <img src="{{ asset('storage/' . $article->img) }}" class="img-fluid rounded mb-4" alt="Article Image">
                        <p class="card-text mb-4"><strong>Description:</strong> {{ $article->desc }}</p>
                        <hr>
                        <p class="card-text">{{ $article->content }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<x-section-footer></x-section-footer>
@endsection



