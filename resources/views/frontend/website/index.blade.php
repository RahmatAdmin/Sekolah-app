@extends('layouts.tamplate')
@section('content')

<header id="header" class="header d-flex align-items-center fixed-top">
  <div class="container-fluid container-xl position-relative d-flex align-items-center">
    <a href="index.html" class="logo d-flex align-items-center me-auto">
      <h1 class="sitename">BAITUL QUR'AN</h1>
    </a>
    <nav id="navmenu" class="navmenu">
      <ul>
        <li><a href="#hero" class="active">Home</a></li>
        <li><a href="#about">Tentang</a></li>
        <li><a href="#services">Takhasus</a></li>
        <li><a href="#portfolio">Gallery</a></li>
        <li><a href="#contact">Contact</a></li>
        <button class="btn btn-outline-success"><a href="{{ route('login') }}">Login Admin</a></button>
      </ul>
      <i class="mobile-nav-toggle d-xl-none bi bi-list"></i>
    </nav>
    <a class="cta-btn" href="{{ route('page.daftar') }}">Pendaftaran</a>
  </div>
</header>


<main class="main">
  <section id="hero" class="hero section dark-background">
    <img src="{{ asset('img/bg.jpeg') }}" alt="" data-aos="fade-in">
    <div class="container d-flex flex-column align-items-center">
      <h2 data-aos="fade-up" data-aos-delay="100">Pondok Pesantren Baitul Qur’an Bina Ukhuwah</h2>
      <p data-aos="fade-up" data-aos-delay="200">Bergabunglah bersama kami dalam lingkungan pesantren yang nyaman,
        mendidik santri menjadi pribadi yang berilmu, beradab, dan berdaya guna bagi umat.</p>
      <div class="d-flex mt-4" data-aos="fade-up" data-aos-delay="300">
        <a href="{{ route('page.daftar') }}" class="btn btn-success">BERGABUNGLAH BERSAMA KAMI</a>
        {{-- <a href="https://www.youtube.com/watch?v=Y7f98aduVJ8" class="glightbox btn-watch-video d-flex align-items-center"><i class="bi bi-play-circle"></i><span>Putar Video</span></a> --}}
      </div>
    </div>
  </section>

  <!-- About Section -->
  <section id="about" class="about section">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
          <h3>PENGANTAR</h3>
          @foreach($data as $dataset)
        @if($dataset->{'dropzone-file'})
      <img src="{{ asset('storage/' . $dataset->{'dropzone-file'}) }}" alt="SKL" class="img-fluid"
      style="width: 350px; height: auto;">
    @else
    <p>Belum ada SKL diunggah.</p>
  @endif
      @endforeach
          {{-- <img src="https://palpres.disway.id/upload/ff959fa7276fabe7c9004456356c0d68.jpeg" class="img-fluid rounded-4 mb-4" alt=""> --}}
          <p style="text-align: justify">Pondok Pesantren Baitul Qur’an Bina Ukhuwah adalah lembaga pendidikan Islam
            yang berfokus pada pembinaan generasi Qur’ani. Dengan menjadikan Al-Qur’an sebagai inti dari setiap aspek
            kehidupan, kami berkomitmen untuk mencetak santri yang tidak hanya menghafal, tetapi juga memahami dan
            mengamalkan nilai-nilai Al-Qur’an dalam kehidupan sehari-hari.</p>
          <p style="text-align: justify">Di bawah bimbingan para asatidz yang berpengalaman, kami menciptakan lingkungan
            belajar yang kondusif, berbasis ukhuwah Islamiyah, serta mengedepankan akhlak mulia. Dengan metode
            pembelajaran yang terstruktur dan mendalam, kami membimbing setiap santri untuk menjadi pribadi yang
            berilmu, beriman, dan siap berkontribusi bagi umat</p>
        </div>
        <div class="col-lg-6" data-aos="fade-up" data-aos-delay="250">
          <div class="content ps-0 ps-lg-5">
            <ul>
              <li><i style="color: green" class="bi bi-check-circle-fill"></i> <span>Tahfidz Al-Qur’an Metode
                  Intensif</span></li>
              <li><i style="color: green" class="bi bi-check-circle-fill"></i> <span>Pemahaman Tafsir & Ulumul
                  Qur’an</span></li>
              <li><i style="color: green" class="bi bi-check-circle-fill"></i> <span>Penguatan Bahasa Arab & Kajian
                  Hadis</span></li>
            </ul>
            <p>
              Bergabunglah dengan Pondok Pesantren Baitul Qur’an Bina Ukhuwah dan jadilah bagian dari generasi Qur’ani
              yang cerdas, berakhlak, dan siap menerangi dunia dengan cahaya ilmu
            </p>
            <button class="btn btn-success"><a class="text-white" href="{{ route('page.daftar') }}">DAFTAR
                DISINI</a></button>
            <div class="position-relative mt-4">
              <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.13.19.jpeg') }}" class="img-fluid rounded-4"
                alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Stats Section -->
  <section id="stats" class="stats section light-background">
    <div class="container" data-aos="fade-up" data-aos-delay="100">
      <div class="row gy-4">
        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i style="color: green" class="bi bi-emoji-smile color-blue flex-shrink-0"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="200" data-purecounter-duration="1"
                class="purecounter"></span>
              <p>SANTRI</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i style="color: green" class="bi bi-journal-richtext color-green flex-shrink-0"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="20" data-purecounter-duration="1"
                class="purecounter"></span>
              <p>PROGRAM</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i style="color: green" class="bi bi-headset color-green flex-shrink-0"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="10" data-purecounter-duration="1"
                class="purecounter"></span>
              <p>PELAYANAN</p>
            </div>
          </div>
        </div><!-- End Stats Item -->

        <div class="col-lg-3 col-md-6">
          <div class="stats-item d-flex align-items-center w-100 h-100">
            <i style="color: green" class="bi bi-people color-pink flex-shrink-0"></i>
            <div>
              <span data-purecounter-start="0" data-purecounter-end="50" data-purecounter-duration="1"
                class="purecounter"></span>
              <p>PENGAJAR</p>
            </div>
          </div>
        </div><!-- End Stats Item -->
      </div>
    </div>
  </section><!-- /Stats Section -->

  <!-- Services Section -->
  <section id="services" class="services section">

    <!-- Section Title -->
    <div class="container section-title" data-aos="fade-up">
      <h2>PONDOK PESANTRENT BAITUL QUR'AN</h2>
      <p>PROGRAM KHUSUS<br></p>
    </div><!-- End Section Title -->
    <div class="container" data-aos="fade-up" data-aos-delay="100">
      <div class="row gy-5">
        <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="200">
          <div class="service-item">
            <div class="img">
              <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.06.18.jpeg') }}" class="img-fluid" alt="">
            </div>
            <div class="details position-relative">
              <a href="service-details.html" class="stretched-link">
                <h3>Tahfidzul Qur'an</h3>
              </a>
              <p>dirancang untuk mencetak generasi penghafal Al-Qur’an yang tidak hanya memiliki hafalan yang kuat,
                tetapi juga memahami makna serta mengamalkan nilai-nilainya dalam kehidupan sehari-hari.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="300">
          <div class="service-item">
            <div class="img">
              <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.20.jpeg') }}" class="img-fluid" alt="">
            </div>
            <div class="details position-relative">
              <a href="service-details.html" class="stretched-link">
                <h3>Tasmi'</h3>
              </a>
              <p>Program Tasmi’ di Pondok Pesantren Baitul Qur’an Bina Ukhuwah adalah program istimewa yang dirancang
                untuk melatih santri dalam memperdengarkan hafalan Al-Qur’an mereka secara lancar dan tartil di hadapan
                guru, teman, maupun jamaah.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-xl-4 col-md-6" data-aos="zoom-in" data-aos-delay="400">
          <div class="service-item">
            <div class="img">
              <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.13.19.jpeg') }}" class="img-fluid" alt="">
            </div>
            <div class="details position-relative">
              <a href="service-details.html" class="stretched-link">
                <h3>Wisuda Tahfidz</h3>
              </a>
              <p>Wisuda Tahfidz di Pondok Pesantren Baitul Qur’an Bina Ukhuwah adalah momen istimewa untuk menghargai
                perjuangan santri dalam menghafal Al-Qur’an. Acara ini bukan sekadar seremoni, tetapi juga menjadi bukti
                keberhasilan santri dalam menyelesaikan hafalan sesuai target yang telah ditetapkan..</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Clients Section -->
  <section id="clients" class="clients section light-background">
    <div class="container" data-aos="fade-up">
      <div class="row gy-4">
        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/logo-kaligrafi-removebg-preview.png') }}" class="img-fluid" alt="">
        </div>

        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/logo-sekolah-kk-removebg-preview.png') }}" class="img-fluid" alt="">
        </div>

        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/logo-sekolah-pp-removebg-preview.png') }}" class="img-fluid" alt="">
        </div>

        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/th-removebg-preview.png') }}" class="img-fluid" alt="">
        </div>

        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/maha-suci.png') }}" class="img-fluid" alt="">
        </div>

        <div class="col-xl-2 col-md-3 col-6 client-logo">
          <img src="{{ asset('img/arr-rahman.png') }}" class="img-fluid" alt="">
        </div>

      </div>
    </div>
  </section>


  <section id="features" class="features section">
    <div class="container">
      <ul class="nav nav-tabs row  d-flex" data-aos="fade-up" data-aos-delay="100">
        <li class="nav-item col-3">
          <a style="background-color: green; color: white; border: 1px solid green;" class="nav-link active show"
            data-bs-toggle="tab" data-bs-target="#features-tab-1">
            <h5 style="color: white">step 1</h5>
          </a>
        </li>
        <li class="nav-item col-3">
          <a style="background-color: green; color: white; border: 1px solid green;" class="nav-link"
            data-bs-toggle="tab" data-bs-target="#features-tab-2">
            <h5 style="color: white">step 2</h5>
          </a>
        </li>
        <li class="nav-item col-3">
          <a style="background-color: green; color: white; border: 1px solid green;" class="nav-link"
            data-bs-toggle="tab" data-bs-target="#features-tab-3">
            <h5 style="color: white">step 3</h5>
          </a>
        </li>
        <li class="nav-item col-3">
          <a style="background-color: green; color: white; border: 1px solid green;" class="nav-link"
            data-bs-toggle="tab" data-bs-target="#features-tab-4">
            <h5 style="color: white">step 4</h5>
          </a>
        </li>
      </ul>

      <div class="tab-content" data-aos="fade-up" data-aos-delay="200">
        <div class="tab-pane fade active show" id="features-tab-1">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>PERSIAPKAN BERKAS PENDAFTARAN</h3>
              <ul>
                <li><i style="color: green;" class="bi bi-check2-all"></i>
                  <spab>pastikan ananda meyiapkan berkas dan persyaratan pendaftaran online.</spab>
                </li>
                <li><i style="color: green;" class="bi bi-check2-all"></i> <span>Foto Copy Kartu Keluarga 1
                    Lembar</span>.</li>
                <li><i style="color: green;" class="bi bi-check2-all"></i> <span>Foto Copy Akte Kelahiran 1
                    Lembar</span></li>
                <li><i style="color: green;" class="bi bi-check2-all"></i> <span>Foto Copy SKL Jika Ada (Boleh
                    Menyusul)</span></li>
                <li><i style="color: green;" class="bi bi-check2-all"></i> <span>Pas Foto Ukuran 3X4 2 Lembar</span>
                </li>
                <li><i style="color: green;" class="bi bi-check2-all"></i> <span>Memenuhi Persyaratan Administrasi yang
                    telah ditentukan</span></li>
              </ul>
              <p>
                Jika terdapatk kendala dan hal yang meragukan lainnya boleh langsung konfirmasi atau tanyakan langsung
                ke admin via whatsapp <button class="btn btn-success">Whatsapp</button>
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="{{ asset('img/ustadz.webp') }}" alt="" class="img-fluid">
            </div>
          </div>
        </div><!-- End Tab Content Item -->

        <div class="tab-pane fade" id="features-tab-2">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>AKSES HALAMAN PENDAFTARAN MELALUI WEBSITE KAMI</h3>
              <p>
                pada halaman website ananda cukup kunjungi halaman pendaftaran dan akan di arahkan ke halaman isi
                formulir
              </p>
              <ul>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Pastikan telah mempersiapkan berkas yang
                    telah di tetapkan</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Data Pribadi</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Data Ayah</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Data Ibu</span></li>
                <p>
                  Jika terdapatk kendala dan hal yang meragukan lainnya boleh langsung konfirmasi atau tanyakan langsung
                  ke admin via whatsapp <button class="btn btn-success">Whatsapp</button>
                </p>
              </ul>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="{{ asset('img/ustadz.webp') }}" alt="" class="img-fluid">
            </div>
          </div>
        </div>

        <div class="tab-pane fade" id="features-tab-3">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>ISI FORMULIR PENDAFTARAN</h3>
              <p>
                Kepada seluruh ananda, kami mengingatkan agar TIDAK LUPA untuk mengisi dan memperhatikan ISI FORMULIR
                PENDAFTARAN dengan benar dan lengkap
              </p>
              <ul>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span> Pastikan semua data terisi dengan
                    benar</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span> Periksa kembali sebelum dikirim.
                  </span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span> Jangan sampai ada yang terlewat!.
                  </span></li>
              </ul>
              <p>
                Jika terdapatk kendala dan hal yang meragukan lainnya boleh langsung konfirmasi atau tanyakan langsung
                ke admin via whatsapp <button class="btn btn-success">Whatsapp</button>
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="{{ asset('img/ustadz.webp') }}" alt="" class="img-fluid">
            </div>
          </div>
        </div><!-- End Tab Content Item -->

        <div class="tab-pane fade" id="features-tab-4">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
              <h3>MENUNGGU KONFIRMASI KELULUSAN VIA WHATSAPP</h3>
              <p>
                Setelah Anda menyelesaikan proses pendaftaran, harap menunggu konfirmasi kelulusan yang akan dikirimkan
                melalui WhatsApp. Pastikan nomor WhatsApp yang Anda daftarkan aktif dan dapat dihubungi.
              </p>
              <p class="fst-italic">
                Mohon bersabar dalam proses verifikasi, karena tim kami akan memeriksa data dengan teliti sebelum
                memberikan pengumuman kelulusan.
              </p>
              <ul>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Pastikan nomor WhatsApp yang Anda
                    daftarkan benar dan aktif.</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Konfirmasi akan dikirimkan dalam waktu
                    yang telah ditentukan oleh panitia.</span></li>
                <li><i style="color: green" class="bi bi-check2-all"></i> <span>Jika dalam batas waktu tertentu Anda
                    belum menerima konfirmasi, silakan hubungi panitia untuk informasi lebih lanjut.</span></li>
              </ul>
              <p>
                Jika terdapatk kendala dan hal yang meragukan lainnya boleh langsung konfirmasi atau tanyakan langsung
                ke admin via whatsapp <button class="btn btn-success">Whatsapp</button>
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center">
              <img src="{{ asset('img/ustadz.webp') }}" alt="Konfirmasi via WhatsApp" class="img-fluid">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="services-2" class="services-2 section light-background">
    <div class="container section-title" data-aos="fade-up">
      <h2>AKTIVITAS</h2>
      <p>FASILITAS PENDIDIKAN</p>
    </div>

    <div class="container">
      <div class="row gy-4">
        <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-book icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Program Tahfidz</a></h4>
              <p class="description">Program unggulan untuk menghafal Al-Qur'an dengan metode yang terstruktur dan
                bimbingan dari para asatidz.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-people icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Kajian Keislaman</a></h4>
              <p class="description">Kajian rutin tentang tafsir, hadits, fiqih, dan akhlak untuk memperdalam pemahaman
                agama Islam.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="300">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-mortarboard icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Pendidikan Formal</a></h4>
              <p class="description">Pondok pesantren menyediakan pendidikan formal berbasis kurikulum nasional yang
                terintegrasi dengan pendidikan diniyah.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="400">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-heart icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Pembinaan Karakter</a></h4>
              <p class="description">Membentuk karakter santri yang disiplin, mandiri, serta berakhlak mulia dalam
                kehidupan sehari-hari.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="500">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-globe icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Bahasa Arab & Inggris</a></h4>
              <p class="description">Program pembelajaran bahasa Arab dan Inggris untuk membekali santri dengan
                kemampuan komunikasi internasional.</p>
            </div>
          </div>
        </div><!-- End Service Item -->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="600">
          <div class="service-item d-flex position-relative h-100">
            <i style="color: green" class="bi bi-calendar4-week icon flex-shrink-0"></i>
            <div>
              <h4 class="title"><a href="#" class="stretched-link">Kegiatan Ekstrakurikuler</a></h4>
              <p class="description">Berbagai kegiatan ekstrakurikuler seperti pramuka, bela diri, dan seni Islami untuk
                mengembangkan bakat santri.</p>
            </div>
          </div>
        </div><!-- End Service Item -->
      </div>
    </div>


  </section>
  <section id="testimonials" class="testimonials section dark-background">
    <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.22.jpeg') }}" class="testimonials-bg" alt="">
    <div class="container" data-aos="fade-up" data-aos-delay="100">
      <div class="swiper init-swiper">
        {{-- <script type="application/json" class="swiper-config">
            {
              "loop": true,
              "speed": 600,
              "autoplay": {
                "delay": 5000
              },
              "slidesPerView": "auto",
              "pagination": {
                "el": ".swiper-pagination",
                "type": "bullets",
                "clickable": true
              }
            }
          </script> --}}
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <div class="testimonial-item">
              <h3>PIMPINAN PONPES</h3>
              <div class="stars">

              </div>
              <p>
                <i class="bi bi-quote quote-icon-left"></i>
                <span>المَعْهَدُ مَوْلِدُ القَادَةِ الَّذِينَ يَجْمَعُونَ بَيْنَ العِلْمِ وَالأَخْلَاقِ</span>
                <br>
                <span>Pesantren adalah tempat lahirnya pemimpin berakhlak dan berilmu.</span>
                <i class="bi bi-quote quote-icon-right"></i>
              </p>
            </div>
          </div><!-- End testimonial item -->

        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
  </section>

  <section id="portfolio" class="portfolio section">
    <div class="container section-title" data-aos="fade-up">
      <h2>KENANGAN</h2>
      <p>GALLERY KENANGAN</p>
    </div>

    <div class="container">
      <div class="isotope-layout" data-default-filter="*" data-layout="masonry" data-sort="original-order">

        <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
          <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
            <div class="portfolio-content h-100">
              <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.06.18.jpeg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <p>PONPES BAITUL QUR'AN</p>
                <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.06.18.jpeg') }}"
                  data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i class="bi bi-zoom-in"></i></a>
              </div>
            </div>
          </div>

          <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
              <div class="portfolio-content h-100">
                <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.07.41.jpeg') }}" class="img-fluid" alt="">
                <div class="portfolio-info">
                  <p>PONPES BAITUL QUR'AN</p>
                  <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.07.41.jpeg') }}"
                    data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                      class="bi bi-zoom-in"></i></a>
                </div>
              </div>
            </div>

            <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
              <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                <div class="portfolio-content h-100">
                  <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.08.20.jpeg') }}" class="img-fluid" alt="">
                  <div class="portfolio-info">
                    <p>PONPES BAITUL QUR'AN</p>
                    <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.08.20.jpeg') }}"
                      data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                        class="bi bi-zoom-in"></i></a>
                  </div>
                </div>
              </div>

              <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
                <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                  <div class="portfolio-content h-100">
                    <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.20.jpeg') }}" class="img-fluid" alt="">
                    <div class="portfolio-info">
                      <p>PONPES BAITUL QUR'AN</p>
                      <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.20.jpeg') }}"
                        data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                          class="bi bi-zoom-in"></i></a>
                    </div>
                  </div>
                </div>

                <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
                  <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                    <div class="portfolio-content h-100">
                      <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.22.jpeg') }}" class="img-fluid" alt="">
                      <div class="portfolio-info">
                        <p>PONPES BAITUL QUR'AN</p>
                        <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.10.22.jpeg') }}"
                          data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                            class="bi bi-zoom-in"></i></a>
                      </div>
                    </div>
                  </div>

                  <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
                    <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                      <div class="portfolio-content h-100">
                        <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.13.19.jpeg') }}" class="img-fluid"
                          alt="">
                        <div class="portfolio-info">
                          <p>PONPES BAITUL QUR'AN</p>
                          <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.13.19.jpeg') }}"
                            data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                              class="bi bi-zoom-in"></i></a>
                        </div>
                      </div>
                    </div>

                    <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
                      <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                        <div class="portfolio-content h-100">
                          <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.14.15.jpeg') }}" class="img-fluid"
                            alt="">
                          <div class="portfolio-info">
                            <p>PONPES BAITUL QUR'AN</p>
                            <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.14.15.jpeg') }}"
                              data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                                class="bi bi-zoom-in"></i></a>
                          </div>
                        </div>
                      </div>

                      <div class="row gy-4 isotope-container" data-aos="fade-up" data-aos-delay="200">
                        <div class="col-lg-4 col-md-6 portfolio-item isotope-item filter-app">
                          <div class="portfolio-content h-100">
                            <img src="{{ asset('img/WhatsApp Image 2025-01-30 at 11.31.33.jpeg') }}" class="img-fluid"
                              alt="">
                            <div class="portfolio-info">
                              <p>PONPES BAITUL QUR'AN</p>
                              <a href="{{ asset('img/WhatsApp Image 2025-01-30 at 11.31.33.jpeg') }}"
                                data-gallery="portfolio-gallery-app" class="glightbox preview-link"><i
                                  class="bi bi-zoom-in"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
  </section>

  
  <section class="w-100 py-5 bg-light">
    <div class="container">
      <h1>ARTIKEL DAN INFORMASI</h1>
      <div id="artikelCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          @foreach($allartikel->chunk(1) as $chunk) <!-- Bagi artikel menjadi kelompok 4 -->
        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
        <div class="row">
          @foreach($chunk as $artikel)
        <div class="col-md-3">
        <div class="card h-100 border-0 shadow-sm">
        <img src="{{ asset('storage/' . $artikel->img) }}" class="card-img-top" alt="Article Image"
        style="height: 200px; object-fit: cover;">

        <div class="card-body d-flex flex-column">
        <h5 class="card-title fw-bold text-success">{{ $artikel->tittle }}</h5>
        <p class="card-text text-dark text-truncate" style="max-height: 60px;">
          {{ $artikel->desc }}
        </p>
        <a href="{{ route('artikel.detail', $artikel->id) }}" class="btn btn-sm btn-success mt-auto">
          Read More
        </a>
        </div>

        <div class="card-footer border-0 bg-transparent text-muted small">
        Published: {{ $artikel->date }}
        </div>
        </div>
        </div>
      @endforeach
        </div>
        </div>
      @endforeach
        </div>

        <!-- Tombol Navigasi -->
        <button class="carousel-control-prev" type="button" data-bs-target="#artikelCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#artikelCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </button>
      </div>
    </div>
  </section>



  <section id="contact" class="contact section">
    <div class="container section-title" data-aos="fade-up">
      <h2>Contact</h2>
      <p>INFRORMATION CONTACT</p>
    </div>

    <div class="container" data-aos="fade-up" data-aos-delay="100">
      <div class="row gy-4">
        <div class="col-lg-6 ">
          <div class="row gy-4">

            <div class="col-lg-12">
              <div class="info-item d-flex flex-column justify-content-center align-items-center" data-aos="fade-up"
                data-aos-delay="200">
                <i class="bi bi-geo-alt"></i>
                <h3>Address</h3>
                <p>Pinggir, Kabupaten Bengkalis, Riau</p>
                <div class="container">
                  <div class="ratio ratio-16x9">
                    <iframe
                      src="https://www.google.com/maps/embed?pb=!1m13!1m8!1m3!1d15956.479801538015!2d101.231018!3d1.072038!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMcKwMDQnMTkuMyJOIDEwMcKwMTMnNTEuNyJF!5e0!3m2!1sid!2sid!4v1738477685004!5m2!1sid!2sid"
                      allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
                    </iframe>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div style="color: green" class="info-item d-flex flex-column justify-content-center align-items-center"
                data-aos="fade-up" data-aos-delay="300">
                <i style="color: green" class="bi bi-telephone"></i>
                <h3>Call Us</h3>
                <p style="color: gray">+62 822-8869-4951</p>
              </div>
            </div>

            <div class="col-md-6">
              <div class="info-item d-flex flex-column justify-content-center align-items-center" data-aos="fade-up"
                data-aos-delay="400">
                <i style="color: green" class="bi bi-envelope"></i>
                <h3>Email Us</h3>
                <p>baitulquran14720@gmail.com</p>
              </div>
            </div>

          </div>
        </div>

        <div class="col-lg-6">
          <form action="forms/contact.php" method="post" class="php-email-form" data-aos="fade-up" data-aos-delay="500">
            <div class="row gy-4">

              <div class="col-md-6">
                <input type="text" name="name" class="form-control" placeholder="Your Name" required="">
              </div>

              <div class="col-md-6 ">
                <input type="email" class="form-control" name="email" placeholder="Your Email" required="">
              </div>

              <div class="col-md-12">
                <input type="text" class="form-control" name="subject" placeholder="Subject" required="">
              </div>

              <div class="col-md-12">
                <textarea class="form-control" name="message" rows="4" placeholder="Message" required=""></textarea>
              </div>

              <div class="col-md-12 text-center">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>

                <button style="background-color: green" type="submit">Send Message</button>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</main>

<footer id="footer" class="footer dark-background">

  <div class="container footer-top">
    <div class="row gy-4">
      <div class="col-lg-4 col-md-6 footer-about">
        <a href="index.html" class="logo d-flex align-items-center">
          <span class="sitename">PONPES BAITUL QUR'AN</span>
        </a>
        <div class="footer-contact pt-3">
          <p>Pinggir, Kabupaten Bengkalis, Riau</p>
          <p class="mt-3"><strong>Phone:</strong> <span>+62 822-8869-4951</span></p>
          <p><strong>Email:</strong> <span>baitulquran14720@gmail.com</span></p>
        </div>
        <div class="social-links d-flex mt-4">
          <a href=""><i class="bi bi-twitter-x"></i></a>
          <a href=""><i class="bi bi-facebook"></i></a>
          <a href=""><i class="bi bi-instagram"></i></a>
          <a href=""><i class="bi bi-linkedin"></i></a>
        </div>
      </div>

      <div class="col-lg-2 col-md-3 footer-links">
        <h4>Useful Links</h4>
        <ul>
          <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="#">Tentang</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="#">Takhasus</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="#">Pendaftaran</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="#">Login</a></li>
        </ul>
      </div>

    </div>
  </div>

  <div class="container copyright text-center mt-4">
    <p>© <span>Copyright</span> <strong class="px-1 sitename">Ponpes Baitul Qur'an Bina Ukhuwah</strong> <span>All
        Rights Reserved</span></p>
    <div class="credits">
    </div>
  </div>
  @endsection