#!/bin/bash

FOLDER_PATH="dev.pesantrentbinaukhuwah.com"

change_files() {
    echo "changing files"

    cd ~/ci_cd

    # Original directory structure
    OLD_DIR="../"

    # New directory structure (change laravel to your actual directory name)
    NEW_DIR="../laravel"

    # File to modify
    FILE="laravel/public/index.php"

    # Backup the original file (optional)
    cp $FILE $FILE.bak

    # Perform the search and replace operation using sed
    sed -i "s|${OLD_DIR}storage/framework/maintenance.php|${NEW_DIR}/storage/framework/maintenance.php|g" $FILE
    sed -i "s|${OLD_DIR}vendor/autoload.php|${NEW_DIR}/vendor/autoload.php|g" $FILE
    sed -i "s|${OLD_DIR}bootstrap/app.php|${NEW_DIR}/bootstrap/app.php|g" $FILE

    echo "index.php modified successfully."
}


backuping_env() {
    cp ~/public_html/${FOLDER_PATH}/.env ~/ci_cd/.env-dev
}

restore_env() {
    cp ~/ci_cd/.env-dev ~/public_html/${FOLDER_PATH}/.env
}

running_clear() {
    cd ~/public_html/${FOLDER_PATH}

    php artisan migrate
    php artisan optimize
    php artisan optimize:clear

    php artisan cache:clear

    php artisan config:cache
    php artisan config:clear

    php artisan event:cache
    php artisan event:clear

    php artisan route:cache
    php artisan route:clear

    php artisan view:cache
    php artisan view:clear
}

cd ~/ci_cd

echo "removing project archive"
rm laravel.tar.gz

# change_files

echo "backuping env"
backuping_env

echo "removing old project"
rm -fr ~/public_html/${FOLDER_PATH}

echo "running composer"
cd ~/ci_cd/laravel && composer install --no-dev

echo "Deploying project"
# cp -fr ~/ci_cd/laravel/public ~/public_html
cp -fr ~/ci_cd/laravel ~/public_html/${FOLDER_PATH}

echo "restoring env"
restore_env

echo "cleaning ci_cd folder"
rm -fr ~/ci_cd/larav*

echo "running clear"
running_clear

echo "done"

