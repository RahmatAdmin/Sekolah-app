<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_login_details', function (Blueprint $table) {
            $table->id();
            // $table->unsignedBigInteger('user_id');
            // $table->unsignedBigInteger('siswa_id')->nullable();
            // $table->unsignedBigInteger('guru_id')->nullable();
            // $table->unsignedBigInteger('role_id');

            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('siswa_id')->references('id')->on('siswa');
            // $table->foreignId('guru_id')->references('id')->on('guru');
            $table->foreignId('role_id')->references('id')->on('roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_login_details');
    }
};
