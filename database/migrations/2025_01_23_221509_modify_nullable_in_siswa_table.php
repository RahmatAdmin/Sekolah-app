<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('siswa', function (Blueprint $table) {
            $table->string('nama_laqob')->nullable()->change();
            $table->string('asal_sekolah')->nullable()->change();
            $table->string('nisn')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('siswa', function (Blueprint $table) {
            $table->string('nama_laqob')->nullable(false)->change();
            $table->string('asal_sekolah')->nullable(false)->change();
            $table->string('nisn')->nullable(false)->change();
        });
    }
};
