<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap');
            $table->string('nama_laqob');
            $table->string('asal_sekolah');
            $table->string('no_telp')->nullable();
            $table->string('jenis_kelamin');
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('nisn');
            $table->string('skl')->nullable();
            $table->string('akte')->nullable();
            $table->string('kk')->nullable();
            $table->string('pas_foto')->nullable()->comment('Image di figma');
            $table->boolean('is_accepted_phase_1')->default(0)->comment("0 = not accepted, 1 = accepted | tahap 1 pendaftaran (validasi data)");
            // ini tidak di palai yang verif le dua
            $table->boolean('is_accepted_phase_2')->default(0)->comment("0 = not accepted, 1 = accepted | tahap 2 pendaftaran (setelah datang ke sekolah)");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswa');
    }
};
