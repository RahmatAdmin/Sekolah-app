<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('log_notifikasis', function (Blueprint $table) {
            $table->id();
            $table->longText('message');
            $table->string('number');
            $table->json('log')->nullable();
            $table->integer('count')->default(0);
            $table->boolean('is_success')->default(0);
            $table->foreignId('siswa_id')->nullable()->references('id')->on('siswa');
            $table->foreignId('orangtua_id')->nullable()->references('id')->on('orangtuas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('log_notifikasis');
    }
};
