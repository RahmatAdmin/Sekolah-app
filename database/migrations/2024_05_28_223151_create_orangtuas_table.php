<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orangtuas', function (Blueprint $table) {
            $table->id();
            $table->string('full_name') -> comment('Nama / Nama Lengkap');
            $table->string('address') -> comment('Alamat');
            $table->string('birth_date') -> comment('Tgl. Lahir');
            $table->string('religion')  -> comment('Agama');
            $table->enum('last_education', ['sd', 'smp', 'sma', 'd1', 'd2', 'd3', 's1', 's2', 's3']) -> comment('Pendidikan Terakhir');
            $table->string('job') -> comment('Pekerjaan');
            // $table->string('phone')->comment('No. Handphone / Telpon') -> unique();
            $table->string('phone')->comment('No. Handphone / Telpon');
            $table->string('income')->comment('Penghasilan');
            $table->string('gender') -> comment('Jenis Kelamin');
            $table->foreignId('siswa_id') -> references('id') -> on('siswa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orangtuas');
    }
};
