<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('roles')->insert([
            'name' => 'admin',
            'has_power' => json_encode([
                'read',
                'create',
                'update',
                'delete',
            ]),
            'detail' => 'Administrator',
        ]);
        DB::table('roles')->insert([
            'name' => 'siswa',
            'has_power' => json_encode([
                'read',
            ]),
            'detail' => 'every student candidate',
        ]);
    }
}
