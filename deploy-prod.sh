#!/bin/bash

change_files() {
    echo "changing files"

    cd ~/public_html

    # Original directory structure
    OLD_DIR="../"

    # New directory structure (change laravel to your actual directory name)
    NEW_DIR="../laravel"

    # File to modify
    FILE="./index.php"

    # Backup the original file (optional)
    cp $FILE $FILE.bak

    # Perform the search and replace operation using sed
    sed -i "s|${OLD_DIR}storage/framework/maintenance.php|${NEW_DIR}/storage/framework/maintenance.php|g" $FILE
    sed -i "s|${OLD_DIR}vendor/autoload.php|${NEW_DIR}/vendor/autoload.php|g" $FILE
    sed -i "s|${OLD_DIR}bootstrap/app.php|${NEW_DIR}/bootstrap/app.php|g" $FILE

    echo "index.php modified successfully."
}


backuping_env() {
    cp ~/laravel/.env ~/ci_cd/.env-prod
}

restore_env() {
    cp ~/ci_cd/.env-prod ~/laravel/.env
}

running_clear() {
    # cd ~/laravel
    echo "Running Clearing PHP"

    echo "running artisan optimize"
    # php artisan migrate
    php artisan optimize
    php artisan optimize:clear

    echo "running artisan cache"
    php artisan cache:clear

    echo "running artisan config:cache"
    php artisan config:cache
    php artisan config:clear

    echo "running artisan event:cache"
    php artisan event:cache
    php artisan event:clear

    echo "running artisan route:cache"
    php artisan route:cache
    php artisan route:clear

    echo "running artisan view:cache"
    php artisan view:cache
    php artisan view:clear
}

composer_install() {
    echo "Running Composer"
    composer dump-autoload --optimize
    composer install --optimize-autoloader --no-dev
}

build_npm() {
    echo "Running NPM"
    npm install
    npm run build
}


# echo "removing project archive"
# rm laravel.tar.gz

# change_files
composer
running_clear
# build_npm

# echo "backuping env"
# backuping_env

# echo "removing old project"
# rm -fr ~/laravel

# echo "running composer"
# cd ~/ci_cd/laravel && composer install --no-dev

# echo "Deploying project"
# cp -fr ~/ci_cd/laravel/public ~/public_html
# cp -fr ~/ci_cd/laravel ~/laravel

# echo "restoring env"
# restore_env

# echo "cleaning ci_cd folder"
# rm -fr ~/ci_cd/larave*

# echo "running clear"
# running_clear

echo "done"

